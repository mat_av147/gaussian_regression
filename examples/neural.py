from numpy.core.numeric import full
from regression.learning.molneural import MolNeuralNetwork
from regression.parsing.molecule import read_molecular_data
from regression.utility.distribute import get_indices
import time

training_size = 100
testing_size = 10
n = 4
l = 2   
full_size = training_size + testing_size
input_dir_location = '../input'
set_list = ['S1']

# read in molecule
start = time.perf_counter()
full_set = read_molecular_data(input_dir_location, set_list, n_max=n, l_max=l)
print("setup time: {:8.4f}s".format(time.perf_counter() - start))

indices = get_indices(full_size, testing_size, 0)

nn = MolNeuralNetwork("atomic_environment", "energy")
nn.load_data(full_set, **indices)
nn.init_layers((20,), **indices)
nn.init_weights()

nn.feedforward()
nn.sum_atoms(**indices)

for i in indices["testing_indices"]:
    print(nn.test_molecule(nn.get_molecule_by_idx(i)), nn.y[i].T)

nn.calc_weights(False, **indices)

for i in indices["testing_indices"]:
    print(nn.test_molecule(nn.get_molecule_by_idx(i)), nn.y[i].T)