from numpy.linalg.linalg import norm
from regression.learning.linear import LinearRegression
from regression.learning.lasso import LassoRegression
from regression.utility.distribute import get_indices
from regression.utility.descriptor import *
from regression.parsing.molecule import read_molecular_data
import time
import numpy as np

# seed for testing purposes
training_size = 219
testing_size = 10
n = 4
l = 2
full_size = training_size + testing_size
input_dir_location = '../input'
set_list = ['S1']

# read in molecule
start = time.perf_counter()
full_set = read_molecular_data(input_dir_location, set_list, n_max=n, l_max=l)
print("setup time: {:8.4f}s".format(time.perf_counter() - start))

base = LinearRegression("surface", "energy")
base_indices = get_indices(229, 10, 219)
t, scale = base.load_data(full_set, **base_indices)
scale *= 418.4
beta_b = base.calc_weights(**base_indices)
base.update_prediction(plot=False, **base_indices)
lasso = LassoRegression("atomic_environment", "energy")

# perform ordinary least squares algorithm
i = 0
iview = 1
mae = []

np.set_printoptions(precision=20)
while i <= full_size-1:
    lasso.lagrange = 10 ** -7.8
    indices = get_indices(full_size, testing_size, i)
    if i // testing_size == iview:
        translate, scale = lasso.load_data(full_set, v=1, **indices)
    else:
        translate, scale = lasso.load_data(full_set, **indices)
        
    scale *= 418.4 # UNITS FROM 10J->kcal

    lasso.y = lasso.y - (base.x @ beta_b)

    lasso.init_weights()
    c1 = lasso.cost(**indices)[0, 0]

    if i == iview * testing_size:
        beta = lasso.calc_weights(plot=False, verbose=1, **indices)
    else:
        beta = lasso.calc_weights(plot=False, **indices)

    c2 = lasso.cost(**indices)[0, 0]

    if i == iview * testing_size:
        lasso.update_prediction(plot=True, **indices)
    else:
        lasso.update_prediction(**indices)

    obj = lasso.get_objective(**indices)
    if i == iview * testing_size:
        quit()
    print("Index={}, COST({:8.3f}->{:8.3f})".format(i//testing_size, c1, c2))
    print("MAE: {:8.3f}".format(obj / scale))
    mae.append(obj / scale)
    i += testing_size

print("FINAL MAE", sum(mae)/len(mae))
