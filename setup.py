import os
from setuptools import setup, find_packages

print("FIND", find_packages())

# Utility function to read the README file.
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "Gaussian Regression",
    version = "0.0.2",
    author = "Matthew Truscott",
    author_email = "matthew.a.truscott@gmail.com",
    description = ("Machine Learning tools"),
    packages=find_packages(),
    include_package_data=True,
    long_description=read('README.md'),
    install_requires=[
        'regex', 'numpy'
    ],
    classifiers=[
        "Development Status :: 3 - Alpha",
    ],
)