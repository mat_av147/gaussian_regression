import numpy as np

class Feature:
    def __init__(self, label: str, val: np.ndarray):
        """Constructor for Feature class

        Args:
            label (str): the name of the feature for reference
            val (np.ndarray): array is 1d for a molecular feature and 2d for an atomic feature
        """
        self.label = label
        self.val = val