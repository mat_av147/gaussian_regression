def get_indices(ntotal: int, ntesting: int, i: int, start=0):
    indices = {}
    full_indices = list(range(start, ntotal))
    testing_indices = full_indices[i:min(i+ntesting, ntotal)]
    training_indices = full_indices
    del training_indices[i:min(i+ntesting, ntotal)]
    indices['training_indices'] = training_indices
    indices['testing_indices'] = testing_indices
    return indices

# if the input data is initially sorted
import random
def get_random_indices(ntotal: int, ntesting: int, seed=None):
    if seed is not None:
        random.seed(seed)
    indices = {}
    full_indices = list(range(ntotal))
    training_indices = []
    testing_indices = sorted(random.sample(full_indices, ntesting))
    for i in full_indices:
        if i not in testing_indices:
            training_indices.append(i)

    indices['training_indices'] = training_indices
    indices['testing_indices'] = testing_indices
    return indices