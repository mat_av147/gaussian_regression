import functools
import time

from numpy.matrixlib.defmatrix import matrix

def matrixtime(nk=0):
    def _matrixtime(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            start = time.perf_counter()
            value = f(*args, **kwargs)
            elapsed = time.perf_counter() - start
            if hasattr(args[0], "timer"):
                for k in range(nk):
                    elapsed -= args[0].timer.kernel_history[-k]
                args[0].timer.matrix += elapsed
                args[0].timer.add_matrix_history(elapsed)
            else:
                print("WARNING: attempting to time an object without a timer!")
            return value
        return wrapper
    return _matrixtime

def kerneltime(f):
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        start = time.perf_counter()
        value = f(*args, **kwargs)
        elapsed = time.perf_counter() - start
        if hasattr(args[0], "timer"):
            args[0].timer.kernel += elapsed
            args[0].timer.add_kernel_history(elapsed)
        else:
            print("WARNING: attempting to time an object without a timer!")
        return value
    return wrapper

class Timer:
    def __init__(self):
        self.matrix = 0.0
        self.kernel = 0.0
        self.history_max = 10
        self.matrix_history = []
        self.kernel_history = []

    def __str__(self):
        # consider implementing a box draw thing to store these
        corner7 = u'\u250c'
        corner9 = u'\u2510'
        corner1 = u'\u2514'
        corner3 = u'\u2518'
        edgeH = u'\u2500'
        edgeV = u'\u2502'

        output = corner7 + "TIMER" + "".join([edgeH * 11]) + corner9 + "\n"
        output += edgeV + "MATRIX: {:8.2f}".format(self.matrix) + edgeV + "\n"
        output += edgeV + "KERNEL: {:8.2f}".format(self.kernel) + edgeV + "\n"
        output += corner1 + "".join([edgeH * 16]) + corner3 + "\n"
        return output

    def add_matrix_history(self, val: float) -> None:
        if len(self.matrix_history) == self.history_max:
            self.matrix_history.pop(0)
        self.matrix_history.append(val)

    def add_kernel_history(self, val: float) -> None:
        if len(self.kernel_history) == self.history_max:
            self.kernel_history.pop(0)
        self.kernel_history.append(val)
