"""
Contains functions that act on a list of molecule objects
"""
import numpy as np
from typing import List, Tuple

from regression.data.molecule import Molecule

def get_feature_from_list(mlist: List[Molecule], label: str, **kwargs):
    training_size = len(mlist)
    training_indices = range(training_size)

    # set by indices
    if 'training_indices' in kwargs:
        training_indices = kwargs['training_indices']
        training_size = len(training_indices)
    
    nfeatures = mlist[0].get_vector_size([label])
    ndatapoints = training_size

    vec = np.zeros((ndatapoints, nfeatures), dtype=float, order='F')
    
    for i, m in enumerate(training_indices):
        elem = mlist[m].get_vector([label])
        if len(elem.shape) != 1:
            raise ValueError("label ({}) does not refer to a 1d array: {}".format(label, elem.shape))
        vec[i, :] = elem

    return vec

def print_feature_from_list(mlist: List[Molecule], label: str):
    for i in range(len(mlist)):
        print(i, mlist[i].get_vector([label]))

# this is most likely deprecated
def normalize_descriptor(mlist: List[Molecule]) -> None:
    max_values = {}
    # store values
    for m in mlist:
        for key, val in m.features.items():
            if key not in max_values:
                max_values[key] = np.max(val)
            else:
                if isinstance(val, float):
                    max_values[key] = max(max_values[key], val)
                elif isinstance(val, np.ndarray):
                    max_values[key] = np.maximum(max_values[key], np.max(val))
                else:
                    print("error: unexpected type in descriptor.molecule.features")

    # update values
    for m in mlist:
        for key, val in m.features.items():
            m.features[key] /= max_values[key]

# this is most likely deprecated
def normalize_atomic(mlist: List[Molecule]) -> None:
    for m in mlist:
        val = m.features["atomic_environment"]
        m.features["atomic_environment"] = (val / np.repeat(np.linalg.norm(
            val, axis=1)[:, np.newaxis], val.shape[1], axis=1))
        m.features["atomic_environment"][:, -1] = 1.0

def get_mean_norm(mlist: List[Molecule], labels: List[str], v=0, **kwargs) -> Tuple[np.ndarray, np.ndarray]:
    """Returns the mean and the norm of each feature defined by labels

    The `mlist` contains a list of molecules, each of which contain multiple features that can be
    selected and combined to the user's convenience. There is some complication in the fact that
    the features can be atomic or molecular. This function will fail if a combination of these is selected!

    Args:
        mlist (List[Molecule]): Molecule list (can be sliced by specifying indices in kwargs)
        labels (List[str]): labels to be concatenated into a single feature array
        v (int, optional): some extra plotting for testing. Defaults to 0.

    Returns:
        Tuple[np.ndarray, np.ndarray]: arrays for the mean and the variance
        output shape is expected to be the number of indexed molecules by the total dimensionality of
        the feature space defined by `labels`
    """
    training_size = len(mlist)
    training_indices = range(training_size)

    # set by indices
    if 'training_indices' in kwargs:
        training_indices = kwargs['training_indices']
        training_size = len(training_indices)

    # initial pass to get minimum/maximum values in feature array
    size = mlist[0].get_vector_size(labels)
    mean = np.zeros((size))
    normsq = np.zeros((size))
    n = 0

    if v:
        import matplotlib.pyplot as plt
        x = []
        xx = []
    
    # flatten over molecules
    for molidx in training_indices:
        val = mlist[molidx].get_vector(labels)
        if v:
            x.append(molidx)
            xx.append(np.max(val[:, 0]))

        # flatten over atoms only if these exist
        if len(val.shape) == 1:
            n_it = 1
            mean_it = val
        else:
            n_it = val.shape[0]
            mean_it = np.mean(val, axis=0)
            
        mean = (n * mean + n_it * mean_it) / (n + n_it)
        n += n_it
    
    # NOTE: here there is some implicit broadcasting, `val` has shape i, j where i is the number of atoms and j is the
    # number of input features. `mean` has shape j and needs to be broadcast per atom
    for molidx in training_indices:
        val = mlist[molidx].get_vector(labels)
        if len(val.shape) > 1:
            if val.shape[0] > 1:
                normsq += np.linalg.norm(val - mean, axis=0) ** 2
            else:
                normsq += (val[0] - mean) ** 2
        else:
            normsq += (val - mean) ** 2
    
    norm = normsq ** 0.5
    
    if v:
        plt.plot(x, xx, '.')
        plt.show()
        
    return mean, norm

def mv_normalize_atomic(mlist: List[Molecule], mean=0.0, variance=1.0) -> None:
    # initial pass to get minimum/maximum values in feature array
    mean1, variance1 = get_mean_var(mlist)
    
    scale = np.sqrt(variance1 / variance)
    translate = mean1 - mean
    for m in mlist:
        m.features["atomic_environment"] -= translate
        m.features["atomic_environment"] /= scale

def cs_normalize_atomic(mlist: List[Molecule], **kwargs) -> None:
    # normalize X (see https://arxiv.org/pdf/2002.05076.pdf Appendix A)
    ntrain = len(kwargs["training_indices"])
    nproperties = 1 # TODO generalize
    training_meanX, _ = get_mean_var(mlist, **kwargs)
    Y = get_descriptor_as_vector(mlist, "energy", **kwargs)
    training_meanY = np.mean(Y)
    normY = np.sum((Y - training_meanY) ** 2) ** 0.5
    scale = ntrain ** 0.5 / (normY * nproperties ** 0.5)
    for m in mlist:
        val = m.features["atomic_environment"]
        normX = np.linalg.norm(val - training_meanX)
        m.features["atomic_environment"] = ntrain ** 0.5 * (val - training_meanX) / normX
        m.features["atomic_environment"][:, -1] = 1.0
        m.features["energy"] = scale * (m.features["energy"] - training_meanY)

    return training_meanY, scale

def cs_normalize_xy(x: np.ndarray, y: np.ndarray):
    # assume x, y from training set
    ntrain = y.shape[0]
    nprop = y.shape[1]
    ymean = np.mean(y)
    ynorm = np.sum((y - ymean) ** 2) ** 0.5
    scale = ntrain ** 0.5 / (ynorm * nprop ** 0.5)
    xmean = np.mean(x, axis=0)
    xnorm = np.sum((x - xmean) ** 2) ** 0.5
    x = ntrain ** 0.5 * (x - xmean) / xnorm
    y = scale * (y - ymean)
    return x, y

def nn_normalize_xy(x: np.ndarray, y: np.ndarray):
    # assume x, y from training set
    ntrain = y.shape[0]
    xmean = np.mean(x, axis=0)
    xnorm = np.sum((x - xmean) ** 2) ** 0.5
    x = ntrain ** 0.5 * (x - xmean) / xnorm
    y = (y - np.min(y)) / (np.max(y) - np.min(y))
    return x, y



    

