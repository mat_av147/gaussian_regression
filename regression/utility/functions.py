import numpy as np
import warnings

warnings.filterwarnings('error')

# note that these expect numpy arrays, but do work for floats too

def sigmoid(x: np.ndarray):
    return 1 / (1 + np.exp(-x))

def dsigmoid(x: np.ndarray):
    return sigmoid(x) * (1 - sigmoid(x))