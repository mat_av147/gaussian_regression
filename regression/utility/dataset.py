import numpy as np
from typing import List, Tuple

from regression.data.molecule import Molecule

def simple_linear_distribution(n: int, noise_variance: float, seed=None) -> Tuple[List[Molecule], float, List[str], List[str]]:
    """Generates a simple linear distribution of molecule descriptors

    Uses the surface and energy features in molecule to create `n` descriptors. The other features should
    be initialized to zero to be optionally set later.

    Args:
        n (int): number of datapoints
        noise_variance (float): the variance of the noise to be added
        seed (int): optional parameter that seeds the random generator for easy testing

    Returns:
        List[Molecule]: the descriptors generated with a simple linear distribution
        float: the constant that relates input (surface) to output (energy)
        List[str]: xlabels
        List[str]: ylabels
    """
    
    # Make deterministic for testing against reference values
    if seed is not None:
        np.random.seed(seed)

    noise = np.random.normal(0.0, noise_variance, (n, 1,))
    beta = np.random.uniform(-1.0, 1.0, (1, 1,))
    x = np.random.uniform(0.0, 10.0, (n, 1,))
    x = np.sort(x, axis=0)
    y = x * beta + noise

    # V is a list of `molecules` in which the descriptors are stored
    V = []
    for i in range(n):
        d = {
            "SURFACE": x[i],
            "ENERGY": y[i]
        }
        m = Molecule.from_python(d)
        V.append(m)
    
    return V, beta, ["SURFACE"], ["ENERGY"], x

def multi_linear_distribution(n: int, nfx: int, noise_variance: float, seed=None) -> Tuple[List[Molecule], float, List[str], List[str]]:
    """Generates a multidimensional linear distribution of molecule descriptors

    Uses the surface and energy features in molecule to create `n` descriptors. The other features should
    be initialized to zero to be optionally set later.

    Args:
        n (int): number of datapoints
        nfx (int): number of dimensions in the environment descriptor
        noise_variance (float): the variance of the noise to be added
        seed (int): optional parameter that seeds the random generator for easy testing

    Returns:
        List[Molecule]: the descriptors generated with a simple linear distribution
        float: the constant that relates input (surface) to output (energy)
        List[str]: xlabels
        List[str]: ylabels
    """
    
    # Make deterministic for testing against reference values
    if seed is not None:
        np.random.seed(seed)

    noise = np.random.normal(0.0, noise_variance, (n, 1,))
    beta = np.random.uniform(-1.0, 1.0, (nfx, 1,))
    x = np.random.uniform(0.0, 10.0, (n, nfx,))
    y = x @ beta + noise

    # V is a list of `molecules` in which the descriptors are stored
    V = []
    for i in range(n):
        d = {
            "SURFACE": x[i, :],
            "ENERGY": y[i]
        }
        m = Molecule.from_python(d)
        V.append(m)
    
    return V, beta, ["SURFACE"], ["ENERGY"]

def multixy_linear_distribution(n: int, nfx: int, nfy: int, noise_variance: float, seed=None) -> Tuple[List[Molecule], float, List[str], List[str]]:
    """Generates a multidimensional linear distribution of molecule descriptors

    Uses the surface and energy features in molecule to create `n` descriptors. The other features should
    be initialized to zero to be optionally set later.

    Args:
        n (int): number of datapoints
        nfx (int): number of dimensions in the input descriptor
        nfy (int): number of dimensions in the output descriptor
        noise_variance (float): the variance of the noise to be added
        seed (int): optional parameter that seeds the random generator for easy testing

    Returns:
        List[Molecule]: the descriptors generated with a simple linear distribution
        float: the constant that relates input (surface) to output (energy)
        List[str]: xlabels
        List[str]: ylabels
    """
    
    # Make deterministic for testing against reference values
    if seed is not None:
        np.random.seed(seed)

    noise = np.random.normal(0.0, noise_variance, (n, nfy))
    beta = np.random.uniform(-1.0, 1.0, (nfx, nfy))
    x = np.random.uniform(0.0, 10.0, (n, nfx,))
    y = x @ beta + noise

    # V is a list of `molecules` in which the descriptors are stored
    V = []
    for i in range(n):
        d = {
            "SURFACE": x[i, :],
            "ENERGY": y[i, :]
        }
        m = Molecule.from_python(d)
        V.append(m)
    
    return V, beta, ["SURFACE"], ["ENERGY"]

def molecular_distribution(n: int, nfx: int, natmax: int, noise_variance: float, seed=None) -> Tuple[List[Molecule], np.ndarray, List[str], List[str]]:
    """Generates a distribution of molecule descriptors

    Uses the environment and energy features in molecule to create `n` descriptors. The other features should
    be initialized to zero to be optionally set later.

    This differs from the simple distribution due to the environment being described per atom

    Args:
        n (int): number of datapoints
        nfx (int): number of dimensions in the environment descriptor
        natmax (int): the maximum number of atoms to be randomly selected between 1 and natmax
        noise_variance (float): the variance of the noise to be added
        seed (int): optional parameter that seeds the random generator for easy testing

    Returns:
        List[Molecule]: the descriptors generated with a simple linear distribution
        np.ndarray: the relationship between input and output
        List[str]: xlabels
        List[str]: ylabels
    """
    # Make deterministic for testing against reference values
    if seed is not None:
        np.random.seed(seed)

    noise = np.random.normal(0.0, noise_variance, (n, 1,))
    beta = np.random.uniform(-1.0, 1.0, (nfx, 1,))
    print(beta)
    if natmax > 1:
        nat = np.random.randint(1, natmax, (n,))
    else:
        nat = np.ones((n,), dtype=int)
    # unlike other distributions, x and y needs to be generated on the fly

    # V is a list of `molecules` in which the descriptors are stored
    V = []
    for i in range(n):
        x = np.random.uniform(0, 10, (nat[i], nfx,))
        y = np.sum(x, axis=0) @ beta + noise[i, :]
        d = {
            "ENVIRONMENT": x,
            "ENERGY": y
        }
        m = Molecule.from_python(d)
        V.append(m)
    
    return V, beta, nat, ["ENVIRONMENT"], ["ENERGY"]

