from typing import Tuple

def get_indices(i: int, l_max: int) -> Tuple[str, str, str]:
    """In the case that the descriptor file does not contain the max indices

    Args:
        i (int): index of descriptor
        l_max (int): maximum l value (extracted previously or provided explicitly)

    Returns:
        Tuple[str, str, str]: n, n^{prime}, l values for descriptor
    """
    n = 0
    nprime = 0
    index = i // l_max
    l = i % l_max
    while index > 0:
        if index > n:
            index -= (n + 1)
            n += 1
        else:
            nprime = index
            index = 0
    return str(n+1), str(nprime+1), str(l)