# from Claverie et al. (see ref 102 in chem reviews 1994, molecular interactions in solution, or p.2059)
def cavitation_energy(mol):
    cav_atom = {
        120: 4.143862247,
        152: 5.593693353,
        155: 5.746204063,
        170: 6.310382468
    }

    sc_arr = mol.get_feature("surface_contribution")
    r_arr = mol.get_feature("radius")
    e = mol.get_feature("energy")[0] / 418.4
    ID = int(mol.get_feature("path").split('/')[-1].split('_')[0])

    e_claverie = 0.0
    for sc, r in zip(sc_arr, r_arr):
        try:
            # conversion to int to remove float errors
            e_claverie += sc * cav_atom[int(r * 100)]
        except KeyError:
            print("radius {} has no entry, skipping...".format(r))
            return ID, 0.0, e

    return ID, e_claverie, e

def surface(mol):

    sc_arr = mol.get_feature("surface_contribution")
    r_arr = mol.get_feature("radius")
    ID = int(mol.get_feature("path").split('/')[-1].split('_')[0])
    surf = mol.get_feature("surface")[0][0]

    s = 0.0
    for sc, r in zip(sc_arr, r_arr):
        s += sc * 4. * (r-1.7) ** 2        

    return ID, s, surf