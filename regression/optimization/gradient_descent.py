import numpy as np
from regression.data.molecule import Molecule
from regression.learning.gaussian import Gaussian
from typing import List, Tuple

# TODO consider generalizing gaussian processes into any ml method
def optimize_parameters(molecule_set: Molecule, gp: Gaussian, verbose=0):
    if verbose:
        print("RUNNING OPTIMIZATION...")
    objective = gp.cost_function(molecule_set)
    # limit
    learning_iterations = 20
    finite_difference = 1e-4
    convergence = 1e-4

    n_params = gp.get_n_params()
    derivs = np.zeros((n_params), dtype=float)
    forward_objective = np.zeros((n_params), dtype=float)
    h = np.ones((n_params)) * finite_difference
    gamma = np.zeros((n_params), dtype=float)
    for i in range(n_params):
        gamma[i] = gp.get_sensitivity_from_idx(i)

    nprint = 5

    for i in range(learning_iterations):
        if verbose and i % nprint == 0:
            print("-" * 32)
            print("ITERATION {:03d} (obj={:12.6f})".format(i, objective))
            print("-" * 32)
        temp_gp = gp.clone()
        param_list = temp_gp.get_param_values()

        # Use 1st order forward difference (error: O(n)) to minimize computation
        for j, val in enumerate(param_list):
            if gamma[j] == 0.0:
                forward_objective[j] = objective
                continue
            temp_gp.set_param_value_by_idx(j, val + h[j])
            if verbose and i % nprint == 0:
                forward_objective[j] = temp_gp.cost_function(molecule_set, verbose=True)
            else:
                forward_objective[j] = temp_gp.cost_function(molecule_set)
            # reset value afterwards
            temp_gp.set_param_value_by_idx(j, val)
            if forward_objective[j] == objective:
                h[j] *= 2.0

        derivs = (forward_objective - objective) / h

        # exit convergence loop if convergence has been reached
        deriv_length = np.linalg.norm(derivs)
        if verbose and i % nprint == 0:
            print("derivatives: ", derivs)
        if deriv_length < convergence:
            break

        # otherwise, update gp with new parameters
        for j, val in enumerate(param_list):
            newval = val + derivs[j] * gamma[j] / (i+1) ** 0.2
            # for now, constrain all parameters to be greater than zero
            # TODO consider individual constraints
            # consider conjugate gradient instead
            if newval < 0.0:
                newval = val
                gamma[j] /= 2
            if verbose and i % nprint == 0:
                print("{:>18}: {:10.4f} -> {:10.4f}".format(j, val, newval))
            gp.set_param_value_by_idx(j, newval)

        # finally calculate new objective function
        if verbose and i % nprint == 0:
            objective = gp.cost_function(molecule_set, verbose=True)
        else:
            objective = gp.cost_function(molecule_set)
    
    if verbose:
        print("-" * 28)
        print("optimized in {:04d} iterations".format(i))
        print("-" * 28)
        for j, param in enumerate(param_list):
            print("{:>18}: {:12.8f}".format(gp.get_label_from_idx(j), param))
        # skip line
        print("")

def parameter_sweep(molecule_set: Molecule, gp: Gaussian, idx: int, bounds: Tuple[float, float], n_points: int,
        verbose=0, plot=True, range_function=np.linspace) -> Tuple[List[float], List[float]]:

    X = []
    Y1 = []
    Y2 = []
    Y3 = []

    temp_gp = gp.clone()
    for val in range_function(bounds[0], bounds[1], n_points):
        temp_gp.set_param_value_by_idx(idx, val)
        X.append(val)
        #print("x=", val)
        #print("k", temp_gp.k(molecule_set, molecule_set))
        c1, c2 = temp_gp.cost_function(molecule_set, get_components=True)
        Y1.append(c1)
        Y2.append(c2)
        Y3.append(c1+c2)
    
    print(X, Y1, Y2, Y3)
    
    if verbose:
        for x, y3 in zip(X, Y3):
            print("{}={}, OBJECTIVE={}".format(idx, x, y3))
    if plot:
        X = np.log(X)
        import matplotlib.pyplot as plt
        plt.plot(X, Y1)
        plt.plot(X, Y2)
        plt.plot(X, Y3)
        plt.show()

    return X, Y3

def parameter_maximum(molecule_set: Molecule, gp: Gaussian, idx: int, xinit: float, xvar: float, n_iter=10, n_points=5,
        verbose=1) -> float:

    max_x = xinit
    range_x = xvar
    
    for i in range(n_iter):
        x, y = parameter_sweep(molecule_set, gp, idx, (max_x-range_x, max_x+range_x), n_points, plot=False)
        max_x = (x[y.index(max(y))])
        range_x /= n_points
        if verbose:
            print("param sweep {}: x={}".format(i, max_x))

    return max_x