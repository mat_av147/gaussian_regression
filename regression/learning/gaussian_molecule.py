import numpy as np
from typing import List, Tuple
import time

from regression.kernel.base import Kernel
from regression.learning.gaussian import Gaussian
from regression.utility.performance import matrixtime, kerneltime
from regression.utility.descriptor import get_feature_from_list
from regression.linear_algebra.linalg import *
from regression.data.molecule import Molecule

class GaussianMolecule(Gaussian):
    def __init__(self, xlabel: List[str], ylabel: List[str], kernel: Kernel, noise_variance: float, core='FORTRAN') -> None:
        super().__init__(xlabel, ylabel, kernel, noise_variance, core)

    def clone(self) -> 'GaussianMolecule':
        new_kernel = self.kernel.clone()
        new_gp = GaussianMolecule(self.xlabels, self.ylabels, new_kernel, self.noise_variance, self.core)
        return new_gp

    @kerneltime
    def k(self, mlist1: List[Molecule], mlist2: List[Molecule], **kwargs) -> np.ndarray:
        len1 = len(mlist1)
        len2 = len(mlist2)
        index1 = range(len1)
        index2 = range(len2)
        if 'training_indices' in kwargs:
            index1 = kwargs['training_indices']
            len1 = len(index1)
        if 'testing_indices' in kwargs:
            index2 = kwargs['testing_indices']
            len2 = len(index2)
            
        result = np.zeros((len1, len2), dtype=float, order='F')

        for i, m1 in enumerate(index1):
            for j, m2 in enumerate(index2):
                # see eq. 12 in Bartok and Csanyi's GAP tutorial review (2020)
                result[i, j] += self.kernel.calc_covariance(
                    mlist1[m1].input.val, 
                    mlist2[m2].input.val)

        return result
    
    def inv(self, mlist: List[Molecule], **kwargs) -> np.ndarray:
        """invert k(X, X)

        This currently uses numpy's inverse function which is not optimal (?)
        The article this is based on suggests implementing better inversion methods (cholesky decomposition)
        if the training set is larger

        Args:
            X (List[DescriptorMolecule]): the training set

        Returns:
            np.ndarray: the inverse of k(X, X)
        """
        n = len(mlist)
        if 'training_indices' in kwargs:
            n = len(kwargs['training_indices'])
        sigma = np.eye(n, order='F') * np.square(self.noise_variance, order='F')
        if self.core == 'NUMPY':
            result = np.linalg.inv(self.k(mlist, mlist, **kwargs) + sigma)
        elif self.core == 'FORTRAN':
            kernel = self.k(mlist, mlist, **kwargs) + sigma
            result = self.matrix_invert(kernel)
        else:
            print("WARNING (molecule.inv): invalid choice for core, default to `fortran`")
            return self.inv(mlist, core='fortran', **kwargs)
        return result

    def det(self, mlist: List[Molecule], **kwargs) -> np.ndarray:
        """get determinant k(X, X)

        This currently uses numpy's determinant function which is probably not optimal?
        For large matrices, this normally returns zero! to resolve increase the noise_variance

        Args:
            X (List[DescriptorMolecule]): the training set

        Returns:
            float: the determinant of k(X, X)
        """
        sigma = np.eye(len(mlist)) * np.square(self.noise_variance)
        result = np.linalg.det(self.k(mlist, mlist, **kwargs) + sigma)
        return result
    
    def cost_function(self, mlist: List[Molecule], **kwargs) -> float:
        """Calculates the cost of the parameters given some training data

        Args:
            mlist (List[DescriptorMolecule]): the training set

        Returns:
            float: the cost function
        """
        def data_fit_term(mlist: List[Molecule]) -> np.ndarray:
            Y = get_feature_from_list(mlist, "output")
            result = -0.5 * np.matmul(np.matmul(Y.T, self.inv(mlist, **kwargs)), Y)
            return result

        def model_complexity_term(mlist: List[Molecule]) -> np.ndarray:
            det = self.det(mlist, **kwargs)
            return -0.5 * np.log(det)

        data_fit = data_fit_term(mlist)
        model_complexity = model_complexity_term(mlist)
        print("cost function", data_fit, model_complexity)
        return data_fit + model_complexity

    @matrixtime(nk=2)
    def posterior(self, training: List[Molecule], testing: List[Molecule], **kwargs) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:

        inv = self.inv(training, training_indices=kwargs['training_indices'], testing_indices=kwargs['training_indices'])
        covar_mixed = self.k(testing, training, training_indices=kwargs['testing_indices'], testing_indices=kwargs['training_indices'])
        covar_test = self.k(testing, testing, training_indices=kwargs['testing_indices'], testing_indices=kwargs['testing_indices'])
        y = get_feature_from_list(training, "output", **kwargs)

        mean = np.matmul(np.matmul(covar_mixed, inv), y)
        covar = covar_test - np.matmul(np.matmul(covar_mixed, inv), covar_mixed.T)

        return mean, covar, covar_mixed

    @matrixtime(nk=1)
    def kernel_ridge_regression(self, training: List[Molecule], **kwargs) -> np.ndarray:
        inv = self.inv(training, **kwargs)
        y = get_feature_from_list(training, "energy")

        alpha = np.matmul(inv, y)

        return alpha
