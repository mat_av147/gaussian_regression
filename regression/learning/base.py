from typing import List, Tuple
import numpy as np
import matplotlib.pyplot as plt
from numpy.core.numeric import full 

from regression.data.molecule import Molecule
from regression.utility.descriptor import get_feature_from_list

class BaseLearning:
    """
    The main function of this object is to provide a way for machine learning algorithms to interface with
    the data input, by providing a standard `load_data` function

    Attributes:
        xlabels (list[str]): contains a list of strings of all the labels that make up the input descriptor. The
            general idea is that the user wants the freedom to select any combination of features for the input.
        ylabels (list[str]): same as above but for the output descriptor. Expected that all of these labels exist
            as members of the descriptor class.
        learning (str): the algorithm used in order to optimize the cost function
        core (str): how should we operate on matrices (accepts NUMPY=numpy routines, or 
            FORTRAN=custom f2py wrapped routines, that may be optimized better for large matrices)
        x (nd.nparray): once the input descriptor has been loaded and prepared (e.g. normalization), it is
            stored in a usable form here. This form is expected to be standardized.
        y (nd.nparray): same as above but for the output descriptor
        xmean (nd.nparray): stored in order to revert the scaling for the x descriptor after normalization
        xscale (nd.nparray): stored in order to revert the scaling for the x descriptor after normalization
        ymean (nd.nparray): stored in order to revert the scaling for the y descriptor after normalization
        yscale (nd.nparray): stored in order to revert the scaling for the y descriptor after normalization
        prediction (nd.nparray): storage of the predicted output
    """
    def __init__(self, xlabel: List[str], ylabel: List[str], learning=None, core='NUMPY'):
        if type(xlabel) is str:
            self.xlabels = [xlabel]
        else:
            self.xlabels = xlabel

        if type(ylabel) is str:
            self.ylabels = [ylabel]
        else:
            self.ylabels = ylabel
        
        self.learning = learning
        self.core = core

        # to be set using load_data method
        self.x = None
        self.y = None
        self.xmean = None
        self.ymean = None
        self.xscale = None
        self.yscale = None

        # to be set once learning is complete
        self.prediction = None

    def load_data(self, data: List[Molecule], verbosity=0, **kwargs):
        """loads in data from a descriptor object into standardized arrays

        Args:
            data (List[Molecule]): A list of desciptor objects, each one describes one molecule
            verbosity (int, optional): verbosity. Defaults to 0. Higher numbers imply more output

        Raises:
            NotImplementedError: combinations of input feature are not supported yet
            NotImplementedError: combinations of output feature are not supported yet
        """
        if len(self.xlabels) > 1:
            raise NotImplementedError
        if len(self.ylabels) > 1:
            raise NotImplementedError

        # x needs to be zeroed because normalization is a pain
        for mol in data:
            mol.set_input(self.xlabels, "additive")
            mol.set_output(self.ylabels)

        x = get_feature_from_list(data, "input")
        y = get_feature_from_list(data, "output")

        ntrain = len(kwargs["training_indices"])
        nproperties = data[0].get_vector_size(self.ylabels)

        xtrain = x[kwargs["training_indices"]]
        self.xmean = np.mean(xtrain, axis=0)
        xnorm = np.sum((xtrain - self.xmean) ** 2, axis=0) ** 0.5
        self.xscale = ntrain ** 0.5 / xnorm

        ytrain = y[kwargs["training_indices"]]
        self.ymean = np.mean(ytrain, axis=0)
        ynorm = np.sum((ytrain - self.ymean) ** 2, axis=0) ** 0.5
        self.yscale = ntrain ** 0.5 / (ynorm * nproperties ** 0.5)

        self.x = self.xscale * (x - self.xmean)
        self.y = self.yscale * (y - self.ymean)

        # sending a warning whenever one of the molecules has an input feature that lies
        # outside the range given by the training set
        if verbosity > 0:        
            extrapolation = set()
            testing = set(kwargs["testing_indices"])
            idx = kwargs["training_indices"]
            extrapolation |= set(np.where(np.any(self.x < np.min(self.x[idx], axis=0), axis=1))[0].flatten())
            extrapolation |= set(np.where(np.any(self.x > np.max(self.x[idx], axis=0), axis=1))[0].flatten())
            print("extrapolation on testing indices:", extrapolation & testing)

    def init_weights(self):
        """initialize the array that stores the weights

        The dimensionality of the weights array is dependent on the shapes of the input and output arrays.
        Assumes `load_data` has been previously called.

        Raises:
            NotImplementedError: To be implemented by child classes
        """
        raise NotImplementedError

    def calc_weights(self, plot=False, **kwargs):
        """fill the array that stores the weights

        `self.learning` defines the learning algorithm used to calculate the weights. 

        Raises:
            NotImplementedError: To be implemented by child classes
        """
        raise NotImplementedError

    def cost(self, **kwargs):
        """calculate the cost of the prediction

        The cost function should be unique for each child class definition

        Raises:
            NotImplementedError: To be implemented by child classes
        """
        raise NotImplementedError

    def grad_cost(self, **kwargs):
        """calculate the grad of the cost of the prediction

        Used in certain learning algorithms

        Raises:
            NotImplementedError: To be implemented by child classes
        """
        raise NotImplementedError

