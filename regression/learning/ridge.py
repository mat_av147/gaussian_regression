# TODO rewrite these learning objects in a more object oriented way (currently this is just copied modified linear.py)
import numpy as np
import matplotlib.pyplot as plt 

from regression.learning.linear import LinearRegression
from regression.linear_algebra.linalg import mi_lu, mm

class RidgeRegression(LinearRegression):
    def __init__(self, xlabel: str, ylabel: str, learning="OLS", lagrange=0.1):
        super().__init__(xlabel, ylabel)
        self.learning = learning
        self.lagrange = lagrange

    def calc_weights(self, plot=False, **kwargs):
        if self.learning == "OLS":
            return self._weights_ols(plot, **kwargs)
        elif self.learning == "GD":
            return self._weights_gd(plot, **kwargs)

    def _weights_ols(self, plot: bool, **kwargs):
        # V contains the input and output vectors
        x = self.x[kwargs["training_indices"]]
        y = self.y[kwargs["training_indices"]]
        ndesc = x.shape[1]

        xx_inv = mi_lu(mm(x.T, x) + self.lagrange * np.eye(ndesc, order='F'))
        xy = mm(x.T, y)
        self.beta = mm(xx_inv, xy)

        if plot:
            for i in range(x.shape[1]):
                for j in range(y.shape[1]):
                    plt.plot(
                        (x[:, i] / self.xscale[i]) + self.xmean[i], 
                        (y[:, j] / self.yscale[j]) + self.ymean[j], '.')
                    plt.savefig("fitx{}y{}".format(i, j))
                    plt.cla()
        
        return self._get_beta_epsilon()

    def _weights_gd(self, plot=False, verbose=0, **kwargs):
        # V contains the input and output vectors
        epochs = 40
        learning_rate = 1e-2
        x = self.x[kwargs["training_indices"]]
        y = self.y[kwargs["training_indices"]]
        ndesc = x.shape[1]

        # TODO fix this
        raise NotImplementedError

        """
        in order to get good results from the ridge regression, the parameter `self.lagrange` needs
        to be optimized. One can either trial and error this, or use some kind of optimization algorithm
        (ideally) to get this. however since the function is non-differentiable (I think? look more into this)
        the problem is a little tricky. Below is an implementation of gradient descent that relies on finite
        differences.
        TODO look into genetic algorithms
        """

        # for i in range(epochs):
        #     # now do the same for lambda+epsilon for a gradient descent optimization for lambda
        #     xx_inv = mi_lu(mm(x.T, x) + (self.lagrange + learning_rate) * np.eye(ndesc, order='F'))
        #     xy = mm(x.T, y)
        #     self.beta = mm(xx_inv, xy)
        #     obj2 = self.cost_function(V, **kwargs)
        #     if verbose:
        #         print('O2', obj2)

        #     # generate beta/eps for lambda=lambda0
        #     xx_inv = mi_lu(mm(x.T, x) + self.lagrange * np.eye(ndesc, order='F'))
        #     xy = mm(x.T, y)
        #     self.beta = mm(xx_inv, xy)
        #     self.update_prediction(V, **kwargs)
        #     obj1 = self.cost_function(V, **kwargs)
        #     if verbose:
        #         print('O1', obj1)

        #     # lambda1 = lambda0 + step_factor * gradient
        #     learning_rate = 1e-18
        #     gradient = (obj1 - obj2) / learning_rate
        #     if verbose:
        #         print("EPS", learning_rate)
        #         print("G", gradient)
            
        #     if new_lagrange == 0 and self.lagrange == 0:
        #         learning_rate /= 10.0
        #     else:
        #         self.lagrange = new_lagrange
        #         learning_rate = self.lagrange / 10.0
        #     if verbose:
        #         print("L", self.lagrange)

        # final beta calculation
        
        return self.beta

    def cost(self, **kwargs):
        x = self.x[kwargs["training_indices"]]
        y = self.y[kwargs["training_indices"]]
        xb = mm(x, self.beta)
        cost = mm((y - xb).T, y - xb) + self.lagrange * mm(self.beta.T, self.beta)
        return cost