# TODO rewrite these learning objects in a more object oriented way (currently this is just copied modified linear.py)
import numpy as np
import matplotlib.pyplot as plt 

from regression.learning.linear import LinearRegression
from regression.linear_algebra.linalg import mi_lu, mm

class LassoRegression(LinearRegression):
    def __init__(self, xlabel: str, ylabel: str, learning="ISTA", lagrange=0.1):
        super().__init__(xlabel, ylabel)
        self.learning = learning
        self.lagrange = lagrange

    def calc_weights(self, plot=False, **kwargs):
        if self.learning == "ORTHO":
            return self._weights_ortho(plot, **kwargs)
        elif self.learning == "GD":
            return self._weights_sgd(plot, **kwargs)
        elif self.learning == "ISTA":
            return self._weights_ista(plot, **kwargs)
        elif self.learning == "FISTA":
            return self._weights_fista(plot, **kwargs)

    def _weights_ortho(self, plot: bool, **kwargs):
        # V contains the input and output vectors
        x = self.x[kwargs["training_indices"]]
        y = self.y[kwargs["training_indices"]]
        #print(y)
        ndesc = x.shape[1]

        xx_inv = mi_lu(mm(x.T, x))
        xy = mm(x.T, y)
        self.beta = mm(xx_inv, xy)
        # https://en.wikipedia.org/wiki/Lasso_(statistics)#Orthonormal_covariates
        self.beta *= max(0, 1 - ndesc * self.lagrange / np.linalg.norm(self.beta))
        
        return self._get_beta_epsilon()

    def _weights_sgd(self, plot=False, **kwargs):
        """Subgradient method

        Behaves similar to the gradient method but for subgradients (since
        the LASSO function is non-differentiable)

        Args:
            plot (bool, optional): extra output for debugging. Defaults to False.

        Raises:
            ValueError: Bad step size choice (valid=fixed/iter)

        Returns:
            (np.ndarray, np.ndarray): beta, epsilon (weights and biases)
        """
        # to get the initial self.beta guess, run ortho (consider another init)
        self._weights_ortho(False, **kwargs)
        #self.params_init(V, **kwargs)

        step_size = 'iter'
        if 'step_size' in kwargs:
            step_size = kwargs['step_size']

        epochs = 50
        learning_rate = 0.001
        cost = np.zeros(epochs+1)
        beta = np.zeros((epochs+1, self.beta.shape[0], self.beta.shape[1]))
        cost[0] = self.cost(**kwargs)[0, 0]
        beta[0, :, :] = self.beta
        
        if plot:
            print("initial obj: {}".format(cost[0]))

        for e in range(epochs):
            grad = self.grad_cost(beta[e, :, :], **kwargs) + self.lagrange * np.sign(beta[e, :, :])
            if step_size == 'fixed':
                beta[e+1, :, :] = beta[e, :, :] - learning_rate * grad
            elif step_size == 'iter':
                beta[e+1, :, :] = beta[e, :, :] - learning_rate * grad / np.sqrt(e+1)
            else:
                raise ValueError('step_size has an invalid value')
            cost[e+1] = self.cost(beta[e+1, :, :], **kwargs)[0, 0]
            if plot and e % 10 == 0:
                print("EPOCH_{:03d} obj: {}".format(e, cost[e]))

        # get best value
        idx = np.unravel_index(np.argmin(cost), cost.shape)
        self.beta = beta[idx]

        if plot:
            plt.plot(cost)
            plt.show()
                
        return self._get_beta_epsilon()
    
    def _soft_threshold(self, x, tk):
        x = np.where(np.abs(x) > (self.lagrange * tk), x, 0.0)
        x -= self.lagrange * tk * np.sign(x)
        return x

    def _weights_ista(self, plot=False, **kwargs):
        """Iterative Shrinkage-Threshold Algorithm

        Another standard algorithm for solving LASSO regression

        Args:
            plot (bool, optional): extra output for debugging. Defaults to False.

        Raises:
            ValueError: bad step size choice (expects fixed/iter)

        Returns:
            (np.ndarray, np.ndarray): beta, epsilon (weights and biases)
        """
        # to get the initial self.beta guess, run ortho (consider another init)
        #self.beta = np.zeros((self.x.shape[1], self.y.shape[1],))
        self._weights_ortho(plot=False, **kwargs)

        step_size = 'iter'
        if 'step_size' in kwargs:
            step_size = kwargs['step_size']

        epochs = 50
        learning_rate = 0.001
        cost = np.zeros(epochs+1)
        beta = np.zeros((epochs+1, self.beta.shape[0], self.beta.shape[1]))
        cost[0] = self.cost(**kwargs)[0, 0]
        beta[0, :, :] = self.beta 
        
        for e in range(epochs):
            if step_size == 'iter':
                m = learning_rate / (np.sqrt(e + 1))
            else:
                m = learning_rate
            grad = self.grad_cost(beta[e, :, :], **kwargs)
            beta[e+1, :, :] = beta[e, :, :] - m * grad
            #if e > 10:
            #    import pdb; pdb.set_trace()
            beta[e+1, :, :] = self._soft_threshold(beta[e+1, :, :], m)
            cost[e+1] = self.cost(beta[e+1, :, :], **kwargs)[0, 0]
            if plot and e % 10 == 0:
                print("EPOCH_{:03d} obj: {}".format(e, cost[e]))

        # get best value
        idx = np.unravel_index(np.argmin(cost), cost.shape)
        self.beta = beta[idx]

        if plot:
            plt.plot(cost)
            plt.show()
                
        return self._get_beta_epsilon()

    def _weights_fista(self, plot=False, **kwargs):
        """Fast Iterative Shrinkage-Threshold Algorithm

        Another standard algorithm for solving LASSO regression

        Args:
            plot (bool, optional): extra output for debugging. Defaults to False.

        Raises:
            ValueError: bad step size choice (expects fixed/iter)

        Returns:
            (np.ndarray, np.ndarray): beta, epsilon (weights and biases)
        """
        # to get the initial self.beta guess, run ortho (consider another init)
        self._weights_ortho(plot=False, **kwargs)

        step_size = 'fixed'
        if 'step_size' in kwargs:
            step_size = kwargs['step_size']

        epochs = 50
        learning_rate = 0.001
        cost = np.zeros(epochs+1)
        beta = np.zeros((epochs+1, self.beta.shape[0], self.beta.shape[1]))
        cost[0] = self.cost(**kwargs)[0, 0]
        beta[0, :, :] = self.beta 
    
        # single ista iteration to get 2-point history
        grad = self.grad_cost(beta[0, :, :], **kwargs)
        beta[1, :, :] = beta[0, :, :] - learning_rate * grad
        beta[1, :, :] = self._soft_threshold(beta[1, :, :], learning_rate)
        cost[1] = self.cost(beta[1, :, :], **kwargs)[0, 0]
        
        for e in range(1, epochs):
            if step_size == 'iter':
                m = learning_rate / (np.sqrt(e + 1))
            else:
                m = learning_rate
            grad = self.grad_cost(beta[e, :, :], **kwargs)
            frac = (e - 1) / (e + 2)
            beta[e+1, :, :] = beta[e, :, :] + frac * (beta[e, :, :] - beta[e-1, :, :]) - m * grad
            beta[e+1, :, :] = self._soft_threshold(beta[e+1, :, :], m)
            cost[e+1] = self.cost(beta[e+1, :, :], **kwargs)[0, 0]
            if plot and e % 10 == 0:
                print("EPOCH_{:03d} obj: {}".format(e, cost[e]))

        # get best value
        idx = np.unravel_index(np.argmin(cost), cost.shape)
        self.beta = beta[idx]

        if plot:
            plt.plot(cost)
            plt.show()
                
        return self._get_beta_epsilon()

    def cost(self, beta=None, **kwargs):
        if beta is None:
            beta = self.beta
        x = self.x[kwargs["training_indices"]]
        y = self.y[kwargs["training_indices"]]
        xb = mm(x, beta)
        cost = ((y - xb).T @ (y - xb)) / 2. + self.lagrange * np.sqrt(mm(beta.T, beta))
        return cost

    def grad_cost(self, beta=None, **kwargs):
        # actually this ignores the second term!
        if beta is None:
            beta = self.beta
        x = self.x[kwargs["training_indices"]]
        y = self.y[kwargs["training_indices"]]
        xb = mm(x, beta)
        dj = -1. * x.T @ (y - xb)
        return dj