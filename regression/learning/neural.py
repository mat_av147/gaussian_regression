from typing import Tuple
import numpy as np

from regression.learning.base import BaseLearning
from regression.utility.functions import sigmoid, dsigmoid

#https://towardsdatascience.com/how-to-build-your-own-neural-network-from-scratch-in-python-68998a08e4f6
#http://deeplearning.stanford.edu/tutorial/supervised/MultiLayerNeuralNetworks/

class NeuralNetwork(BaseLearning):    
    def __init__(self, xlabel: str, ylabel: str, learning="GD"):
        """Constructor

        Args:
            xlabel (str): label that determines the input feature space
            learning (str, optional): choice of learning mode. Defaults to "OLS".

            x: features
            y: output to train for
            prediction: output predicted by the model
        """
        super().__init__(xlabel, ylabel)
        self.learning = learning
        self.lagrange = 0.0
        self.layers = []            # list of numpy arrays, each element describes a layer of the nn
        self.weights = []           # list of numpy arrays, each element describes the connections between two successive layers
        self.biases = []
        self.derivs = []
        self.gradw = []
        self.gradb = []

    def init_layers(self, hidden_layer_shape: Tuple, **kwargs):
        if len(self.layers) > 0:
            print("layers already exist! resetting...")
            self.layers.clear()

        if 'training_indices' in kwargs:
            self.layers.append(self.x[kwargs['training_indices']])
        else:
            self.layers.append(self.x)
        # to make the indexing for this more logical
        self.derivs.append(None)

        ntraining = self.layers[0].shape[0]
        for n in hidden_layer_shape:
            layer = np.zeros((ntraining, n), dtype=float)
            deriv = np.zeros((ntraining, n), dtype=float)
            self.layers.append(layer)
            self.derivs.append(deriv)

        # setting this to the initial values of y is sort of redundant, could just get the shape and then set to zero
        if 'training_indices' in kwargs:
            self.layers.append(self.y[kwargs['training_indices']])
        else:
            self.layers.append(self.y)
        self.derivs.append(np.zeros(self.layers[-1].shape, dtype=float))
    
    def init_weights(self):
        if len(self.weights) > 0:
            print("weights already exist! resetting...")
            self.weights.clear()
        if len(self.biases) > 0:
            print("biases already exist! resetting...")
            self.biases.clear()
        if len(self.gradw) > 0:
            print("gradw already exists! resetting...")
            self.gradw.clear()
        if len(self.gradb) > 0:
            print("gradb already exists! resetting...")
            self.gradb.clear()
        
        heights = []
        for l in self.layers:
            heights.append(l.shape[1])

        for i in range(len(heights)-1):
            np.random.seed(0) # TODO remove once tested
            self.weights.append(np.random.randn(heights[i], heights[i+1]))
            self.biases.append(0.01 * np.random.randn(1, heights[i+1]))
            self.gradw.append(np.zeros((heights[i], heights[i+1]), dtype=float))
            self.gradb.append(np.zeros((1, heights[i+1]), dtype=float))

    def calc_weights(self, plot=False, **kwargs):
        if self.learning == "GD":
            return self._weights_gd(plot, **kwargs)

    def _weights_gd(self, plot, **kwargs):
        # uses some iterative optimization scheme
        epochs = 500
        learning_rate = 0.1
        ntraining = self.layers[0].shape[0]

        for i in range(epochs):
            self.feedforward(i)
            if i % 25 == 0:
                print("epoch={}, cost={}".format(i, self.cost(**kwargs)))
                if plot:
                    self.draw_graph(0, 'EPOCH_{}'.format(i))
            self.backpropagate(i, **kwargs)

            # apply gradient descent
            for i, (w, gw) in enumerate(zip(self.weights, self.gradw)):
                self.weights[i][:, :] -= learning_rate * (gw / ntraining + self.lagrange * w)
            for i, gb in enumerate(self.gradb):
                self.biases[i][:, :] -= learning_rate * (gb / ntraining)

    def feedforward(self, epoch=None):

        nl = len(self.layers)
        assert len(self.derivs) == nl
        assert len(self.weights) == nl - 1

        for l in range(nl - 1):
            # print(self.layers[l].shape, self.weights[l].shape, self.biases[l].shape)
            z = self.layers[l] @ self.weights[l] + self.biases[l]
            # if epoch % 25 == 0:
            #     print("z={}".format(z))
            self.layers[l+1][:, :] = sigmoid(z)
            try:
                self.derivs[l+1][:, :] = dsigmoid(z)
            except Warning:
                print("layer:\n{}\nweights:\n{}\nbiases:\n{}".format(self.layers[l], self.weights[l], self.biases[l]))
                raise

    def backpropagate(self, epoch=None, **kwargs):

        if 'training_indices' in kwargs:
            y = self.y[kwargs['training_indices']]
        else:
            y = self.y

        nl = len(self.layers)
        assert len(self.derivs) == nl
        assert len(self.weights) == nl - 1

        deltas = [None] * nl
        # layer nl TODO: generalize the cost function to optimize
        deltas[nl - 1] = -1.0 * (y - self.layers[nl - 1]) * self.derivs[nl - 1]
        #print(epoch, deltas[nl - 1], self.layers[nl - 1])
        # everything else
        for i in range(nl - 2, 0, -1):
            #print(deltas[i+1].shape, self.weights[i].shape, self.derivs[i].shape)
            deltas[i] = (deltas[i+1] @ self.weights[i].T) * self.derivs[i]
            #print(epoch, self.weights[i], self.derivs[i], deltas[i])
        
        # print('deltas') 
        # for i in deltas:
        #     print(i)

        for i in range(nl - 1):
            #print(self.gradw[i].shape, deltas[i+1].shape, self.layers[i].shape)
            self.gradw[i][:, :] = np.sum(deltas[i+1][:, None, :] * self.layers[i][:, :, None], axis=0)[:, :]
            self.gradb[i][:, :] = np.sum(deltas[i+1][:, np.newaxis, :], axis=0)[:, :]

    def cost(self, **kwargs):
        y = self.y[kwargs["training_indices"]]
        xb = self.layers[-1][kwargs["training_indices"]]
        j = (y - xb).T @ (y - xb)
        return j[0, 0]

    def draw_graph(self, dataidx: int, label: str):
        from graphviz import Digraph

        g = Digraph('G', filename='{}.gv'.format(label), format='png')

        for i, l in enumerate(self.layers):
            for j, val in enumerate(l[dataidx, :]):
                g.node("n{}{}".format(i, j), label="{:4.3f}".format(val))
            if i+1 < len(self.layers):
                g.node("b{}".format(i), label="1.0")

        for i, w in enumerate(self.weights):
            for j, val in np.ndenumerate(w):
                g.edge("n{}{}".format(i, j[0]), "n{}{}".format(i+1, j[1]), label="{:4.3f}".format(val))
        for i, b in enumerate(self.biases):
            for j, val in np.ndenumerate(b):
                g.edge("b{}".format(i), "n{}{}".format(i+1, j[1]), label="{:4.3f}".format(val))

        g.render()

