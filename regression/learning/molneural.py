from matplotlib import pyplot as plt
from regression.utility.descriptor import get_descriptor_as_vector, get_mean_var
from regression.descriptor.base import Descriptor
from typing import List, Tuple
import numpy as np

from regression.learning.base import BaseLearning
from regression.utility.functions import sigmoid, dsigmoid

#https://towardsdatascience.com/how-to-build-your-own-neural-network-from-scratch-in-python-68998a08e4f6
#http://deeplearning.stanford.edu/tutorial/supervised/MultiLayerNeuralNetworks/

class MolNeuralNetwork(BaseLearning):    
    def __init__(self, xlabel: str, ylabel: str, learning="GD"):
        """Constructor

        Args:
            xlabel (str): label that determines the input feature space
            learning (str, optional): choice of learning mode. Defaults to "OLS".

            x: features
            y: output to train for
            prediction: output predicted by the model
        """
        super().__init__(xlabel, ylabel)
        self.learning = learning
        self.lagrange = 0.0
        self.layers = []            # list of numpy arrays, each element describes a layer of the nn
        self.weights = []           # list of numpy arrays, each element describes the connections between two successive layers
        self.biases = []
        self.derivs = []
        self.gradw = []
        self.gradb = []
        self.prediction = None
        self.nat = None

    def load_data(self, V: List[Descriptor], v=0, add_bias=False, **kwargs):
        # load atomic data
        full_size = len(V)

        # normalize X (see https://arxiv.org/pdf/2002.05076.pdf Appendix A)
        ntrain = len(kwargs["training_indices"])
        nproperties = 1 # TODO generalize

        if add_bias:
            x1shape = (V[0].get_shape(self.xlabel, False)[-1] + 1,)
        else:
            x1shape = (V[0].get_shape(self.xlabel, False)[-1],)
        yshape = (V[0].get_shape(self.ylabel)[-1],)

        self.nat = np.zeros((full_size), dtype=int)
        self.y = np.zeros((full_size,) + yshape, dtype=float, order='F')
        self.prediction = np.zeros((ntrain,) + yshape, dtype=float, order='F')

        x0size = 0
        for i in range(full_size):
            self.nat[i] = V[i].get_shape(self.xlabel, False)[0]
            x0size += self.nat[i]
            self.y[i] = V[i].get_feature(self.ylabel)

        meanx, _ = get_mean_var(V, self.xlabel, v=v, **kwargs) 
        normx = np.linalg.norm(get_descriptor_as_vector(V, self.xlabel, **kwargs) - meanx)

        self.x = np.zeros((x0size,) + x1shape, dtype=float, order='F')
        self.y = (self.y - np.min(self.y)) / (np.max(self.y) - np.min(self.y))
        self.y *= self.nat[:, np.newaxis]
        self.y /= np.max(self.y / self.nat[:, np.newaxis])

        idx = 0
        for i in range(full_size):
            x = V[i].features[self.xlabel]
            
            x = ntrain ** 0.5 * (x - meanx) / normx
            for j in range(x.shape[0]):
                self.x[idx+j] = x[j, :]
                if add_bias:
                    self.x[idx+j, -1] = 1.0
            idx += self.nat[i]

        if v:
            for i in kwargs["testing_indices"]:
                plt.plot(self.x[i, :])
            plt.show()

    def get_subset(self, **kwargs):
        if not 'training_indices' in kwargs:
            print("training_indices must be submitted")
            raise ValueError
        subset = np.zeros((np.sum(self.nat[kwargs['training_indices']]), self.x.shape[1],), dtype=float)
        fullidx = 0
        subidx = 0
        for i, n in np.ndenumerate(self.nat):
            if i[0] in kwargs['training_indices']:
                subset[subidx:subidx+n, :] = self.x[fullidx:fullidx+n, :]
                fullidx += n
                subidx += n
            else:
                fullidx += n
        return subset

    def get_molecule_by_idx(self, idx):
        subset = np.zeros((np.sum(self.nat[idx]), self.x.shape[1],), dtype=float)
        fullidx = 0
        for i, n in np.ndenumerate(self.nat):
            if i[0] == idx:
                subset[0:n, :] = self.x[fullidx:fullidx+n, :]
                fullidx += n
            else:
                fullidx += n
        return subset

    def expand_array(self, arr, **kwargs):
        if not 'training_indices' in kwargs:
            print("training_indices must be submitted")
            raise ValueError
        expand = np.zeros((np.sum(self.nat[kwargs['training_indices']]), arr.shape[1],), dtype=float)
        fullidx = 0
        subidx = 0
        j = 0
        for i, n in np.ndenumerate(self.nat):
            if i[0] in kwargs['training_indices']:
                expand[subidx:subidx+n, :] = arr[j, :]
                fullidx += n
                subidx += n
                j += 1
            else:
                fullidx += n
        return expand


    def init_layers(self, hidden_layer_shape: Tuple, **kwargs):
        if len(self.layers) > 0:
            print("layers already exist! resetting...")
            self.layers.clear()

        if 'training_indices' in kwargs:
            self.layers.append(self.get_subset(**kwargs))
        else:
            self.layers.append(self.x)
        # to make the indexing for this more logical
        self.derivs.append(None)

        ntraining = self.layers[0].shape[0]
        for n in hidden_layer_shape:
            layer = np.zeros((ntraining, n), dtype=float)
            deriv = np.zeros((ntraining, n), dtype=float)
            self.layers.append(layer)
            self.derivs.append(deriv)

        # setting this to the initial values of y is sort of redundant, could just get the shape and then set to zero
        self.layers.append(np.zeros((ntraining, self.y.shape[1]), dtype=float))
        self.derivs.append(np.zeros(self.layers[-1].shape, dtype=float))
    
    def init_weights(self):
        if len(self.weights) > 0:
            print("weights already exist! resetting...")
            self.weights.clear()
        if len(self.biases) > 0:
            print("biases already exist! resetting...")
            self.biases.clear()
        if len(self.gradw) > 0:
            print("gradw already exists! resetting...")
            self.gradw.clear()
        if len(self.gradb) > 0:
            print("gradb already exists! resetting...")
            self.gradb.clear()
        
        heights = []
        for l in self.layers:
            heights.append(l.shape[1])

        for i in range(len(heights)-1):
            np.random.seed(0) # TODO remove once tested
            self.weights.append(np.random.randn(heights[i], heights[i+1]))
            self.biases.append(0.01 * np.random.randn(1, heights[i+1]))
            self.gradw.append(np.zeros((heights[i], heights[i+1]), dtype=float))
            self.gradb.append(np.zeros((1, heights[i+1]), dtype=float))

    def calc_weights(self, plot=False, **kwargs):
        if self.learning == "GD":
            return self._weights_gd(plot, **kwargs)

    def _weights_gd(self, plot, **kwargs):
        # uses some iterative optimization scheme
        epochs = 50000
        learning_rate = 0.15
        print("learning_rate", learning_rate)
        ntraining = self.layers[0].shape[0]

        ex = []
        ey = []
        for i in range(epochs):
            self.feedforward(i)
            self.sum_atoms(i, **kwargs)
            if i % 100 == 0:
                ex.append(i)
                ey.append(self.cost(**kwargs))
                print("(EPOCH, COST): {:07d}, {:10.4f}".format(ex[-1], ey[-1]))
            self.backpropagate(i, **kwargs)

            # apply gradient descent
            for i, (w, gw) in enumerate(zip(self.weights, self.gradw)):
                self.weights[i][:, :] -= learning_rate * (gw / ntraining + self.lagrange * w)
            for i, gb in enumerate(self.gradb):
                self.biases[i][:, :] -= learning_rate * (gb / ntraining)
        
        if plot:
            plt.plot(ex, ey)

    def feedforward(self, epoch=None):

        nl = len(self.layers)
        assert len(self.derivs) == nl
        assert len(self.weights) == nl - 1

        for l in range(nl - 1):
            # print(self.layers[l].shape, self.weights[l].shape, self.biases[l].shape)
            z = self.layers[l] @ self.weights[l] + self.biases[l]
            # if epoch % 25 == 0:
            #     print("z={}".format(z))
            self.layers[l+1][:, :] = sigmoid(z)
            try:
                self.derivs[l+1][:, :] = dsigmoid(z)
            except Warning:
                print("layer:\n{}\nweights:\n{}\nbiases:\n{}".format(self.layers[l], self.weights[l], self.biases[l]))
                raise

    def sum_atoms(self, epoch=None, **kwargs):
        # combines last layer into a prediction array
        if 'training_indices' in kwargs:
            nat = self.nat[kwargs['training_indices']]
        else:
            nat = self.nat
        self.prediction.fill(0.0)
        idx = 0
        for i, n in np.ndenumerate(nat):
            self.prediction[i[0], :] = np.sum(self.layers[-1][idx:idx+n])
            idx += n

    def backpropagate(self, epoch=None, **kwargs):

        if 'training_indices' in kwargs:
            y = self.y[kwargs['training_indices']]
            nat = self.nat[kwargs['training_indices']]
        else:
            y = self.y
            nat = self.nat

        nl = len(self.layers)
        assert len(self.derivs) == nl
        assert len(self.weights) == nl - 1

        deltas = [None] * nl
        # layer nl TODO: generalize the cost function to optimize
        deltas[nl - 1] = -1.0 * self.expand_array(y - self.prediction, **kwargs) * self.derivs[nl - 1]
        #print(epoch, deltas[nl - 1], self.layers[nl - 1])
        # everything else
        for i in range(nl - 2, 0, -1):
            #print(deltas[i+1].shape, self.weights[i].shape, self.derivs[i].shape)
            deltas[i] = (deltas[i+1] @ self.weights[i].T) * self.derivs[i]
            #print(epoch, self.weights[i], self.derivs[i], deltas[i])
        
        # print('deltas') 
        # for i in deltas:
        #     print(i)

        for i in range(nl - 1):
            #print(self.gradw[i].shape, deltas[i+1].shape, self.layers[i].shape)
            self.gradw[i][:, :] = np.sum(deltas[i+1][:, None, :] * self.layers[i][:, :, None], axis=0)[:, :]
            self.gradb[i][:, :] = np.sum(deltas[i+1][:, np.newaxis, :], axis=0)[:, :]

    def cost(self, **kwargs):
        y = self.y[kwargs["training_indices"]]
        xb = self.prediction
        j = (y - xb).T @ (y - xb)
        return j[0, 0]

    def draw_graph(self, dataidx: int, label: str):
        from graphviz import Digraph

        g = Digraph('G', filename='{}.gv'.format(label), format='png')

        for i, l in enumerate(self.layers):
            for j, val in enumerate(l[dataidx, :]):
                g.node("n{}{}".format(i, j), label="{:4.3f}".format(val))
            if i+1 < len(self.layers):
                g.node("b{}".format(i), label="1.0")

        for i, w in enumerate(self.weights):
            for j, val in np.ndenumerate(w):
                g.edge("n{}{}".format(i, j[0]), "n{}{}".format(i+1, j[1]), label="{:4.3f}".format(val))
        for i, b in enumerate(self.biases):
            for j, val in np.ndenumerate(b):
                g.edge("b{}".format(i), "n{}{}".format(i+1, j[1]), label="{:4.3f}".format(val))

        g.render()
    
    def test_molecule(self, x):
        nl = len(self.layers)

        for l in range(nl - 1):
            # print(self.layers[l].shape, self.weights[l].shape, self.biases[l].shape)
            z = x @ self.weights[l] + self.biases[l]
            # if epoch % 25 == 0:
            #     print("z={}".format(z))
            x = sigmoid(z)
        
        return np.sum(x, axis=0)