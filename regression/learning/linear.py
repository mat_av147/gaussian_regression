import numpy as np
import matplotlib.pyplot as plt

from regression.learning.base import BaseLearning

class LinearRegression(BaseLearning):
    def __init__(self, xlabel: str, ylabel: str, learning="OLS"):
        super().__init__(xlabel, ylabel)
        if self.core == "FORTRAN":
            from regression.linear_algebra.linalg import mi_lu, mm
            self.matrix_multiply = mm
            self.matrix_invert = mi_lu
        self.beta = None
        self.learning = learning

    def init_weights(self):
        nfx = self.x.shape[1]
        nfy = self.y.shape[1]
        self.beta = np.zeros((nfx, nfy), order='F')

    def calc_weights(self, plot=False, **kwargs):
        if self.learning == "OLS":
            return self._weights_ols(plot, **kwargs)
        elif self.learning == "GD":
            return self._weights_gd(plot, **kwargs)

    def _weights_ols(self, plot: bool, **kwargs):
        """calculate weights via ordinary least squares method

        Args:
            plot (bool): plot the x/y fit for each dimension (WARNING: generates a lot of graphs if there 
            are many descriptors!)

        Returns:
            (np.ndarray, np.ndarray): weights and biases, transformed back to unnormalized units
        """

        # V contains the input and output vectors
        x = self.x[kwargs["training_indices"]]
        y = self.y[kwargs["training_indices"]]

        if self.core == "FORTRAN":
            xx_inv = self.matrix_invert(self.matrix_multiply(x.T, x))
            xy = self.matrix_multiply(x.T, y)
            self.beta = self.matrix_multiply(xx_inv, xy)
        elif self.core == "NUMPY":
            xx_inv = np.linalg.inv(x.T @ x)
            xy = x.T @ y
            self.beta = xx_inv @ xy
        else:
            raise ValueError("core not recognized")

        if plot:
            for i in range(x.shape[1]):
                for j in range(y.shape[1]):
                    plt.plot(
                        (x[:, i] / self.xscale[i]) + self.xmean[i], 
                        (y[:, j] / self.yscale[j]) + self.ymean[j], '.')
                    plt.savefig("fitx{}y{}".format(i, j))
                    plt.cla()

        return self._get_beta_epsilon()
    
    def _weights_gd(self, plot: bool, **kwargs):
        """calculate weights via gradient descent method

        Args:
            plot (bool): plot the x/y fit for each dimension (WARNING: generates a lot of graphs if there 
            are many descriptors!)

        Returns:
            (np.ndarray, np.ndarray): weights and biases, transformed back to unnormalized units
        """

        learning_rate = 1e-5
        epochs = 100
        # run ols to get an initial guess
        self.params_ols(False, **kwargs)
        #self.params_init(V, **kwargs)
        init_cost = self.cost(**kwargs)[0, 0]
        x = []
        y = []
        x.append(-1)
        y.append(init_cost)
        for e in range(epochs):
            self.beta[:, 0] -= learning_rate * self.grad_cost(V, **kwargs)
            x.append(e)
            y.append(self.cost(**kwargs)[0, 0])
        if plot:
            plt.plot(x, y)
            print(y)
            plt.show()

        return self._get_beta_epsilon()

    def _get_beta_epsilon(self, beta=None):
        """scales the weights and biases to unnormalized units

        Args:
            beta (np.ndarray, optional): weights to override self stored weights. Defaults to None.

        Returns:
            (np.ndarray, np.ndarray): weights and biases
        """
        if beta is None:
            beta = self.beta

        xyscale = np.divide.outer(self.xscale, self.yscale)
        beta = xyscale * beta

        # NOTE: the math here might be wrong..
        epsilon = self.ymean - (self.xmean @ beta)

        return beta, epsilon

    def cost(self, **kwargs):
        x = self.x[kwargs["training_indices"]]
        y = self.y[kwargs["training_indices"]]

        if self.core == "FORTRAN":
            xb = self.matrix_multiply(x, self.beta)
        elif self.core == "NUMPY":
            xb = x @ self.beta
        else:
            raise ValueError("core not recognized")

        j = (y - xb).T @ (y - xb)
        return j

    def grad_cost(self, **kwargs):
        x = self.x[kwargs["training_indices"]]
        y = self.y[kwargs["training_indices"]]

        if self.core == "FORTRAN":
            xb = self.matrix_multiply(x, self.beta)
            dj = -2. * self.matrix_multiply((y - xb.T), x)
        elif self.core == "NUMPY":
            xb = x @ self.beta
            dj = -2. * ((y - xb.T) @ x)
        else:
            raise ValueError("core not recognized")

        return dj[0, :]

    def update_prediction(self, plot=False, **kwargs):
        """Calculates the predicted output

        Required in order to calculate objective function, and also provides a
        convenient plot option to check how good the prediction is based on the
        current calculated weights and biases

        Args:
            plot (bool, optional): Plot the predicted result vs the actual result, which
            should be stored internally for the training_set. Defaults to False.

        Returns:
            (np.ndarray, np.ndarray): predicted output and actual output
        """
        if self.beta is None:
            print("beta not set!")
            return

        self.prediction = (self.x @ self.beta)

        if plot:
            itr = kwargs["training_indices"]
            itt = kwargs["testing_indices"]
            plt.plot(
                (self.prediction[itr] / self.yscale) + self.ymean, 
                (self.y[itr] / self.yscale) + self.ymean, '.')
            plt.plot(
                (self.prediction[itt] / self.yscale) + self.ymean,
                (self.y[itt] / self.yscale) + self.ymean, '.')
            plt.xlabel("predicted cavitation energy")
            plt.ylabel("actual cavitation energy")
            plt.show()
        
        return self.prediction, self.y

    def get_prediction(self):
        """Assuming the `update_prediction` method has been called, this should return the predicted output in
        unnormalized units

        Returns:
            np.ndarray: predicted output
        """
        return self.prediction / self.yscale + self.ymean

    def get_objective(self, **kwargs):
        """Calculates the objective function (simply the mean absolute error)

        Returns:
            np.ndarray: objective function
        """
        if self.prediction is None:
            print("prediction has not been calculated! attempting to remedy..")
            self.update_prediction()
        
        y = self.y[kwargs["testing_indices"]] / self.yscale + self.ymean
        prediction = self.prediction[kwargs["testing_indices"]] / self.yscale + self.ymean

        objective = np.mean(np.abs(y - prediction))
        return objective
