from regression.utility.descriptor import get_feature_from_list, get_mean_norm
import numpy as np
from typing import List, Tuple
from regression.kernel.base import Kernel, Parameter
from regression.utility.performance import Timer
from regression.utility.performance import matrixtime, kerneltime
from regression.data.molecule import Molecule

class Gaussian:
    def __init__(self, xlabel: List[str], ylabel: List[str], kernel: Kernel, noise_variance: float, core='NUMPY') -> None:
        if type(xlabel) is str:
            self.xlabels = [xlabel]
        else:
            self.xlabels = xlabel

        if type(ylabel) is str:
            self.ylabels = [ylabel]
        else:
            self.ylabels = ylabel

        self.kernel = kernel
        self.timer = Timer()
        self.core = core
        if self.core == "FORTRAN":
            from regression.linear_algebra.linalg import mi_lu
            self.matrix_invert = mi_lu

        # extra parameters
        self.param_labels = {
            "noise_variance": Parameter(0, 1, 0.0, 0.1)
        }
        self.param_vals = [noise_variance]

        self.xmean = None
        self.ymean = None
        self.xscale = None
        self.yscale = None

    def load_data(self, data: List[Molecule], verbose=0, yscale=None, **kwargs):
        for mol in data:
            mol.set_input(self.xlabels, "none")
            mol.set_output(self.ylabels)

        #x = get_feature_from_list(data, "input")
        y = get_feature_from_list(data, "output")
        
        # operation needs to be performed per atom, normalization
        # should be accounted for not here but in the calculation of the covariate matrix
        # However, a zero mean is required so that the dot product calculation is valid. 
        self.xmean, _ = get_mean_norm(data, self.xlabels, **kwargs)
        self.xscale = 1.

        ntrain = len(kwargs["training_indices"])
        nproperties = data[0].get_vector_size(self.ylabels)

        ytrain = y[kwargs["training_indices"]]
        self.ymean = np.mean(ytrain, axis=0)
        ynorm = np.sum((ytrain - self.ymean) ** 2, axis=0) ** 0.5
        if yscale is not None:
            # override the automatic scaling of y
            self.yscale = yscale
        else:
            self.yscale = ntrain ** 0.5 / (ynorm * nproperties ** 0.5)

        for mol in data:
            mol.set_input_from_vector(self.xscale * (mol.get_vector(self.xlabels) - self.xmean), "none")
            mol.set_output_from_vector(self.yscale * (mol.get_vector(self.ylabels) - self.ymean))

        # sending a warning whenever one of the molecules has an input feature that lies
        # outside the range given by the training set
        if verbose > 0:        
            extrapolation = set()
            testing = set(kwargs["testing_indices"])
            idx = kwargs["training_indices"]
            extrapolation |= set(np.where(np.any(self.x < np.min(self.x[idx], axis=0), axis=1))[0].flatten())
            extrapolation |= set(np.where(np.any(self.x > np.max(self.x[idx], axis=0), axis=1))[0].flatten())
            print("extrapolation on testing indices:", extrapolation & testing)

    def get_param_labels(self) -> List[str]:
        param_list = self.kernel.get_param_labels()
        return param_list + self.param_labels.keys()

    def get_param_values(self) -> List[float]:
        return self.kernel.get_param_values() + self.param_vals

    def set_param_value_by_idx(self, idx, val) -> None:
        kernel_n_params = self.kernel.get_n_params()
        if idx >= kernel_n_params:
            idx -= kernel_n_params
            self.param_vals[idx] = val
        else:
            self.kernel.set_param_value_by_idx(idx, val)

    def get_n_params(self, param_list=None) -> int:
        if param_list is None:
            return len(self.param_vals) + self.kernel.get_n_params()
        n = 0
        for param in self.param_labels:
            if param in param_list:
                n += self.param_labels[param].length
        return self.kernel.get_n_params(param_list) + n

    def get_param_value(self, label: str) -> float:
        if label in self.param_labels:
            param = self.param_labels[label]
            value = self.param_vals[param.idx:param.idx+param.length]
        else:
            value = self.kernel.get_param_value(label)
        return value
    
    def print_params(self):
        for key, val in self.param_labels.items():
            print(key, val)
        for key, val in self.kernel.param_labels.items():
            print(key, val)

    def set_param_value(self, label: str, value: float) -> None:
        if label in self.param_labels:
            param = self.param_labels[label]
            if len(value) != param.length:
                raise ValueError("value shape does not match the expected parameter shape")
            for i in range(param.length):
                self.param_vals[param.idx+i] = value[i]
        else:
            self.kernel.set_param_value(label, value)

    def set_sensitivity(self, label: str, sensitivity: float) -> None:
        if label in self.param_labels:
            if self.param_labels[label] == 0.0:
                print("sensitivity should not be non zero, ignoring request")
                return
            self.param_labels[label].sensitivity = sensitivity
        else:
            self.kernel.set_sensitivity(label, sensitivity)

    def get_sensitivity_from_idx(self, idx: int) -> float:
        label = self.get_label_from_idx(idx)
        if label in self.param_labels:
            return self.param_labels[label].sensitivity
        else:
            return self.kernel.param_labels[label].sensitivity
    
    def get_label_from_idx(self, idx: int) -> str:
        if idx >= self.kernel.get_n_params():
            idx -= self.kernel.get_n_params()
            for key, val in self.param_labels.items():
                if idx >= val.idx and idx < val.idx + val.length:
                    return key
        else:
            return self.kernel.get_label_from_idx(idx)

    def clone(self) -> 'Gaussian':
        new_kernel = self.kernel.clone()
        # TODO clone the param vals in a better way
        new_gp = Gaussian(self.xlabels, self.ylabels, new_kernel, self.param_vals[0], self.core)
        return new_gp

    @kerneltime
    def k(self, mlist1: List[Molecule], mlist2: List[Molecule], **kwargs) -> np.ndarray:
        len1 = len(mlist1)
        len2 = len(mlist2)
        index1 = range(len1)
        index2 = range(len2)
        if 'training_indices' in kwargs:
            index1 = kwargs['training_indices']
            len1 = len(index1)
        if 'testing_indices' in kwargs:
            index2 = kwargs['testing_indices']
            len2 = len(index2)
            
        result = np.zeros((len1, len2), dtype=float, order='F')

        for i, m1 in enumerate(index1):
            for j, m2 in enumerate(index2):
                # see eq. 12 in Bartok and Csanyi's GAP tutorial review (2020)
                result[i, j] += self.kernel.calc_covariance(
                    mlist1[m1].input.val, 
                    mlist2[m2].input.val, self.kernel.get_param_values(), self.kernel.get_param_dict())
                    
        return result
    
    def inv(self, data: List[Molecule], **kwargs) -> np.ndarray:
        """invert k(X, X)

        This currently uses numpy's inverse function which is not optimal (?)
        The article this is based on suggests implementing better inversion methods (cholesky decomposition)
        if the training set is larger

        Args:
            X (List[DescriptorMolecule]): the training set

        Returns:
            np.ndarray: the inverse of k(X, X)
        """
        n = len(data)
        if 'training_indices' in kwargs:
            n = len(kwargs['training_indices'])
        sigma = np.eye(n, order='F') * np.square(self.get_param_value("noise_variance"), order='F')
        if self.core == 'NUMPY':
            result = np.linalg.inv(self.k(data, data, **kwargs) + sigma)
        elif self.core == 'FORTRAN':
            kernel = self.k(data, data, **kwargs) + sigma
            result = self.matrix_invert(kernel)
        else:
            print("WARNING (molecule.inv): invalid choice for core, default to `fortran`")
            return self.inv(data, core='fortran', **kwargs)
        return result

    def det(self, data: List[Molecule], **kwargs) -> np.ndarray:
        """get determinant k(X, X)

        This currently uses numpy's determinant function which is probably not optimal?
        For large matrices, this normally returns zero! to resolve increase the noise_variance

        Args:
            X (List[DescriptorMolecule]): the training set

        Returns:
            float: the determinant of k(X, X)
        """
        sigma = np.eye(len(data)) * np.square(self.get_param_value("noise_variance"))
        result = np.linalg.det(self.k(data, data, **kwargs) + sigma)
        return result
    
    def cost_function(self, data: List[Molecule], verbose=False, get_components=False, **kwargs) -> float:
        """Calculates the cost of the parameters given some training data

        Args:
            data (List[DescriptorMolecule]): the training set

        Returns:
            float: the cost function
        """
        def data_fit_term(data: List[Molecule]) -> np.ndarray:
            y = get_feature_from_list(data, "output")
            result = -0.5 * (y.T @ (self.inv(data, **kwargs) @ y))[0, 0]
            return result

        def model_complexity_term(data: List[Molecule]) -> np.ndarray:
            det = self.det(data, **kwargs)
            return -0.5 * np.log(det)

        data_fit = data_fit_term(data)
        model_complexity = model_complexity_term(data)
        if verbose:
            print("cost function = {:8.3f} + {:8.3f} = {:12.5f}".format(data_fit, model_complexity, data_fit + model_complexity))
        if get_components:
            return data_fit, model_complexity
        return data_fit + model_complexity

    def error_function(self, data: List[Molecule], verbose=0, denorm=True, **kwargs) -> float:
        y = get_feature_from_list(data, "output")[kwargs["testing_indices"]]
        y_pred, _, _ = self.posterior(data, data, **kwargs)
        if denorm:
            y = y / self.yscale + self.ymean
            y_pred = y_pred / self.yscale + self.ymean
        if verbose:
            print("predicted y")
            print(y_pred)
            print("actual y")
            print(y)
        return np.linalg.norm((y - y_pred), axis=0)[0] / y.shape[0]

    @matrixtime(nk=2)
    def posterior(self, training: List[Molecule], testing: List[Molecule], **kwargs) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        inv = self.inv(training, training_indices=kwargs['training_indices'], testing_indices=kwargs['training_indices'])
        covar_mixed = self.k(testing, training, training_indices=kwargs['testing_indices'], testing_indices=kwargs['training_indices'])
        covar_test = self.k(testing, testing, training_indices=kwargs['testing_indices'], testing_indices=kwargs['testing_indices'])
        y = get_feature_from_list(training, "output", **kwargs)

        mean = np.matmul(np.matmul(covar_mixed, inv), y)
        covar = covar_test - np.matmul(np.matmul(covar_mixed, inv), covar_mixed.T)

        return mean, covar, covar_mixed

    @matrixtime(nk=1)
    def kernel_ridge_regression(self, training: List[Molecule], **kwargs) -> np.ndarray:
        inv = self.inv(training, **kwargs)
        y = get_feature_from_list(training, "output")

        alpha = np.matmul(inv, y)

        return alpha
