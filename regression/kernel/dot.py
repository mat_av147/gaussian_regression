from regression.kernel.base import Kernel, Parameter
import numpy as np
from copy import deepcopy

class DotKernel(Kernel):
    def __init__(self, signal_variance: float = None, power: float = None) -> None:
        super().__init__()
        self.calcs = [self.covariance]
        self.param_labels["signal_variance"] = Parameter(0, 1, 0.0, 0.1)
        self.param_labels["power"] = Parameter(1, 1, 1.0, 0.0)
        if signal_variance is None:
            signal_variance = self.param_labels["signal_variance"].default
        if power is None:
            power = self.param_labels["power"].default
        self.param_vals = [signal_variance, power]

    def clone(self) -> 'DotKernel':
        new_kernel = DotKernel(
            self.get_param_value("signal_variance"), self.get_param_value("power"))
        new_kernel.calcs = deepcopy(self.calcs)
        new_kernel.param_vals = deepcopy(self.param_vals)
        new_kernel.param_labels = deepcopy(self.param_labels)
        new_kernel.operators = deepcopy(self.operators)
        return new_kernel

    def covariance(self, X1: np.ndarray, X2: np.ndarray, param_vals, param_labels, verbose, **kwargs) -> float:
        assert X1.ndim == 1
        assert X2.ndim == 1
        assert "signal_variance" in param_labels
        assert "power" in param_labels
        # number of descriptors should be consistent
        assert X1.shape[0] == X2.shape[0]

        result = np.square(self.get_param_value("signal_variance", param_vals, param_labels)) + np.dot(X1, X2)
        xl1 = np.sqrt(np.dot(X1, X1))
        xl2 = np.sqrt(np.dot(X2, X2))

        # these vectors should never be zero
        assert xl1 > 0.0
        assert xl2 > 0.0

        result /= (xl1 * xl2)
        result = result ** self.get_param_value("power", param_vals, param_labels)

        return result

class MoleculeDotKernel(Kernel):
    def __init__(self, signal_variance: float, power: float) -> None:
        super().__init__()
        self.calcs = [self.covariance]
        self.param_labels["signal_variance"] = Parameter(0, 1, 0.0, 0.1)
        self.param_labels["power"] = Parameter(1, 1, 1.0, 0.0)
        if signal_variance is None:
            signal_variance = self.param_labels["signal_variance"].default
        if power is None:
            power = self.param_labels["power"].default
        self.param_vals = [signal_variance, power]

    def clone(self) -> 'MoleculeDotKernel':
        new_kernel = MoleculeDotKernel(
            self.get_param_value("signal_variance"), self.get_param_value("power"))
        new_kernel.calcs = deepcopy(self.calcs)
        new_kernel.param_vals = deepcopy(self.param_vals)
        new_kernel.param_labels = deepcopy(self.param_labels)
        new_kernel.operators = deepcopy(self.operators)
        return new_kernel

    def covariance(self, X1: np.ndarray, X2: np.ndarray, param_vals, param_labels, verbose, **params) -> float:
        assert X1.ndim == 2
        assert X2.ndim == 2
        assert "signal_variance" in param_labels
        assert "power" in param_labels
        # number of descriptors should be consistent
        assert X1.shape[1] == X2.shape[1]
        xn1 = np.zeros(X1.shape[0])
        xn2 = np.zeros(X2.shape[0])
        dot = np.zeros((X1.shape[0], X2.shape[0]))
        if self.core == 'broadcast':
            X1 = np.moveaxis(X1, 0, 1)
            X2 = np.moveaxis(X2, 0, 1)
            dot = np.sum(X1[:, :, np.newaxis] * X2[:, np.newaxis, :], axis=0)
            xn1[:] = np.sqrt(np.sum(X1 * X1, axis=0))
            xn2[:] = np.sqrt(np.sum(X2 * X2, axis=0))
        else:
            for index in np.ndindex(dot.shape):
                dot[index] = np.sum(X1[index[0]] * X2[index[1]])      
            xn1[:] = np.sqrt(np.sum(X1 * X1, axis=1))
            xn2[:] = np.sqrt(np.sum(X2 * X2, axis=1))
        result = np.square(self.get_param_value("signal_variance", param_vals, param_labels)) + dot
        xn = np.multiply.outer(xn1, xn2)
        result /= (xn)
        result = result ** self.get_param_value("power", param_vals, param_labels)
        result = np.sum(result)
        return result
