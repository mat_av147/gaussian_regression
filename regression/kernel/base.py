import operator
import numpy as np
from copy import deepcopy
from typing import List, Tuple
from dataclasses import dataclass

@dataclass
class Parameter:
    idx: int
    length: int
    default: float
    sensitivity: float

class Kernel:
    def __init__(self):
        self.calcs = [self.covariance]
        self.param_labels = {}
        self.param_vals = []
        self.operators = [operator.add]
        self.core = 'default'

    def __add__(self, other) -> 'Kernel':
        new_kernel = Kernel()
        new_kernel.calcs = self.calcs + other.calcs
        new_kernel.operators.append(operator.add)
        new_kernel.merge_params(self, other)
        return new_kernel
    
    def __multiply__(self, other) -> 'Kernel':
        new_kernel = Kernel()
        new_kernel.calcs = self.calcs + other.calcs
        new_kernel.operators.append(operator.multiply)
        new_kernel.merge_params(self, other)
        return new_kernel

    def merge_params(self, k1: 'Kernel', k2: 'Kernel') -> None:
        n = len(k1.param_vals)
        self.param_vals = k1.param_vals + k2.param_vals
        for key, val in k1.param_labels.items():
            self.param_labels[key] = val
        for key, val in k2.param_labels.items():
            val.idx += n
            self.param_labels[key] = val

    def set_sensitivity(self, label: str, sensitivity: float) -> None:
        if self.param_labels[label] == 0.0:
            print("sensitivity should not be non zero, ignoring request")
            return
        self.param_labels[label].sensitivity = sensitivity

    def get_sensitivity_from_idx(self, idx: int) -> float:
        label = self.get_label_from_idx(idx)
        return self.param_labels[label].sensitivity
    
    def get_label_from_idx(self, idx: int) -> str:
        for key, val in self.param_labels.items():
            if idx >= val.idx and idx < val.idx + val.length:
                return key

    def get_param_labels(self) -> List[str]:
        return self.param_labels.keys()
    
    def get_param_values(self) -> List[float]:
        return self.param_vals

    def get_param_dict(self) -> dict:
        return self.param_labels
    
    def get_n_params(self, param_list=None) -> int:
        if param_list is None:
            return len(self.param_vals)
        n = 0
        for label in self.param_labels:
            if label in param_list:
                n += self.param_labels[label].length
        return n

    def get_param_value(self, label: str, param_vals=None, param_labels=None) -> float:
        if param_labels is None:
            param_labels = self.param_labels
        if param_vals is None:
            param_vals = self.param_vals
        if label in param_labels:
            param = param_labels[label]
            return param_vals[param.idx:param.idx+param.length]
        raise AttributeError

    def set_param_value_by_idx(self, idx: int, val: float) -> None:
        self.param_vals[idx] = val

    def set_param_value(self, label: str, value: float) -> None:
        if label in self.param_labels:
            param = self.param_labels[label]
            if len(value) != param.length:
                raise ValueError("value shape does not match the expected parameter shape")
            for i in range(param.length):
                self.param_vals[param.idx+i] = value[i]
            return
        raise AttributeError

    def clone(self) -> 'Kernel':
        new_kernel = Kernel()
        new_kernel.calcs = deepcopy(self.calcs)
        new_kernel.param_vals = deepcopy(self.param_vals)
        new_kernel.param_labels = deepcopy(self.param_labels)
        new_kernel.operators = deepcopy(self.operators)
        return new_kernel

    def covariance(self, X1: np.ndarray, X2: np.ndarray, param_vals, param_labels) -> float:
        raise NotImplementedError

    def calc_covariance(self, X1: np.ndarray, X2: np.ndarray, param_vals=None, param_labels=None, verbose=0) -> float:
        if param_vals is None:
            param_vals = self.param_vals
        if param_labels is None:
            param_labels = self.param_labels
        assert len(self.calcs) == len(self.operators)
        result = 0
        for calc, op in zip(self.calcs, self.operators):
            result = op(result, calc(X1, X2, param_vals, param_labels, verbose))
        return result