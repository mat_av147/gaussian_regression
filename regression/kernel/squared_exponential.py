from regression.kernel.base import Kernel, Parameter
import numpy as np
from typing import List
from copy import deepcopy

class SquaredExponentialKernel(Kernel):
    def __init__(self, lengthscale: List[float] = None, signal_variance: float = None) -> None:
        super().__init__()
        self.calcs = [self.covariance]
        if type(lengthscale) == float:
            lengthscale = [lengthscale]
        self.param_labels["lengthscale"] = Parameter(0, len(lengthscale), 1.0, 0.1)
        self.param_labels["signal_variance"] = Parameter(len(lengthscale), 1, 0.0, 0.1)
        if signal_variance is None:
            signal_variance = self.param_labels["signal_variance"].default
        if lengthscale is None:
            lengthscale = [self.param_labels["lengthscale"].default]
        self.param_vals = lengthscale + [signal_variance]

    def clone(self) -> 'SquaredExponentialKernel':
        new_kernel = SquaredExponentialKernel(
            self.get_param_value("lengthscale"), self.get_param_value("signal_variance"))
        new_kernel.calcs = deepcopy(self.calcs)
        new_kernel.param_vals = deepcopy(self.param_vals)
        new_kernel.param_labels = deepcopy(self.param_labels)
        new_kernel.operators = deepcopy(self.operators)
        return new_kernel

    def covariance(self, X1: np.ndarray, X2: np.ndarray, param_vals, param_labels, verbose=0) -> float:
        assert X1.ndim == 1
        assert X2.ndim == 1
        # number of features should be consistent
        assert X1.shape == X2.shape
        if verbose:
            print("covariance", param_vals) 
        l = param_labels["lengthscale"].length
        if l != X1.shape[0]:
            # there is a mismatch between the lengthscale parameter and the vector
            # if the length used to be 1, assume that the input intended this lengthscale
            # to be broadcasted. Otherwise error
            if l != 1:
                raise ValueError("array length {} does not match the lengthscale parameter {}".format(X1.shape[0], l))
            # otherwise infer the desired lengthscale parameter and shift the other parameters in the array
            i = param_labels["lengthscale"].idx
            param_labels["lengthscale"].length = X1.shape[0]
            add_slice = [param_vals[i]] * (X1.shape[0] - 1)
            param_vals[i+1:i+1] = add_slice  

        signal_variance = np.array(self.get_param_value("signal_variance", param_vals, param_labels))
        lengthscale = self.get_param_value("lengthscale", param_vals, param_labels)
        
        if type(lengthscale) == float and lengthscale < 1e-10:
            return 0. 
        elif type(lengthscale) == list:
            for ls in lengthscale:
                if ls < 1e-10:
                    return 0.

        diff = (X1 - X2) / lengthscale
        result = np.square(signal_variance) * np.exp(-0.5 * (diff @ diff.T))
        return result

class MoleculeSquaredExponentialKernel(Kernel):
    def __init__(self, lengthscale: List[float] = None, signal_variance: float = None) -> None:
        super().__init__()
        self.core = "broadcast"
        self.calcs = [self.covariance]
        if type(lengthscale) == float:
            lengthscale = [lengthscale]
        self.param_labels["lengthscale"] = Parameter(0, len(lengthscale), 1.0, 0.1)
        self.param_labels["signal_variance"] = Parameter(len(lengthscale), 1, 0.0, 0.1)
        if signal_variance is None:
            signal_variance = self.param_labels["signal_variance"].default
        if lengthscale is None:
            lengthscale = [self.param_labels["lengthscale"].default]
        self.param_vals = lengthscale + [signal_variance]

    def clone(self) -> 'SquaredExponentialKernel':
        new_kernel = SquaredExponentialKernel(
            self.get_param_value("lengthscale"), self.get_param_value("signal_variance"))
        new_kernel.calcs = deepcopy(self.calcs)
        new_kernel.param_vals = deepcopy(self.param_vals)
        new_kernel.param_labels = deepcopy(self.param_labels)
        new_kernel.operators = deepcopy(self.operators)
        return new_kernel
    
    def covariance(self, X1: np.ndarray, X2: np.ndarray, param_vals, param_labels, verbose=0) -> float:
        assert X1.ndim == 2
        assert X2.ndim == 2
        # number of features should be consistent
        assert X1.shape[1] == X2.shape[1]
        if verbose:
            print("covariance", param_vals) 
        l = param_labels["lengthscale"].length
        if l != X1.shape[0]:
            # there is a mismatch between the lengthscale parameter and the vector
            # if the length used to be 1, assume that the input intended this lengthscale
            # to be broadcasted. Otherwise error
            if l != 1:
                raise ValueError("array length {} does not match the lengthscale parameter {}".format(X1.shape[0], l))
            # otherwise infer the desired lengthscale parameter and shift the other parameters in the array
            i = param_labels["lengthscale"].idx
            param_labels["lengthscale"].length = X1.shape[0]
            add_slice = [param_vals[i]] * (X1.shape[0] - 1)
            param_vals[i+1:i+1] = add_slice 

        diff = np.zeros((X1.shape[0], X2.shape[0]))
        signal_variance = np.array(self.get_param_value("signal_variance", param_vals, param_labels))
        lengthscale = self.get_param_value("lengthscale", param_vals, param_labels) 

        if type(lengthscale) == float and lengthscale < 1e-10:
            return 0. 
        elif type(lengthscale) == list:
            for ls in lengthscale:
                if ls < 1e-10:
                    return 0.

        if self.core == 'broadcast':
            X1 = np.moveaxis(X1, 0, 1)
            X2 = np.moveaxis(X2, 0, 1)
            diff = X1[:, :, np.newaxis] - X2[:, np.newaxis, :]
            diff /= lengthscale
            diff = np.sum(np.square(diff), axis=0)
        else:
            raise ValueError("unknown core selected")

        result = np.square(signal_variance) * np.sum(np.exp(-0.5 * diff))
        return result
