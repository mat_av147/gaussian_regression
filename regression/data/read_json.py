import time
import random
import os
import json
from typing import List

from regression.data.molecule import Molecule

def read_molecular_data(
        input_dir_location: str, 
        set_list: List, 
        fmax=0, 
        verbose=0, 
        n_max=64, 
        l_max=64) -> List[Molecule]:
    full_set = []
    file_ignore = ['.gitignore', 'index.json']
    random.seed(18)
    file_list = []
    fidx = 0

    for s in set_list:
        if fidx >= fmax and fmax > 0:
            break

        files = os.listdir("{}/{}/".format(input_dir_location, s))

        for filename in files:
            if fidx >= fmax and fmax > 0:
                break

            if filename in file_ignore:
                continue

            fidx += 1
            file_list.append("{}/{}/{}".format(input_dir_location, s, filename))

    random.shuffle(file_list)

    start = time.perf_counter()

    for filename in file_list:
        f = open(filename, 'r')
        try:
            m = Molecule.from_json(json.load(f), n_max, l_max)
        except UnicodeDecodeError:
            print(filename)
            quit()
        full_set.append(m)
        f.close()

    if verbose > 0:
        print("setup time: {:8.4f}s".format(time.perf_counter() - start))

    return full_set