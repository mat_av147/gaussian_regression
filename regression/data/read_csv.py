import csv
from descriptor.base import Descriptor

def read_csv_data(rel_path):
    desc = []
    n = 0
    with open('input/{}'.format(rel_path), 'r') as f:
        reader = csv.reader(f, quoting=csv.QUOTE_NONNUMERIC)
        for i, row in enumerate(reader):
            # sanity check data to make sure everything has consistent length
            if i == 0:
                n = len(row)
            else:
                assert len(row) == n
            # might want to change this, currently sets y value as last entry
            desc.append(Descriptor(row[:-1], row[-1]))
    
    return desc