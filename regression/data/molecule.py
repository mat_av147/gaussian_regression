import numpy as np
from typing import List

from regression.features.base import Feature
from regression.utility.spherical import get_indices

class Molecule:
    def __init__(self, path: str, surface: np.ndarray, volume: np.ndarray, energy: np.ndarray, environment: np.ndarray,
            surface_contribution: np.ndarray, radius: np.ndarray):
        """Default Constructor

        Args:
            surface (np.ndarray): Surface descriptor
            volume (np.ndarray): Volume descriptor
            energy (np.ndarray): Energy (output value)
            atomic_environment (np.ndarray): Atomic descriptors
        """
        self.path = path
        self.surface = Feature("surface", surface)
        self.volume = Feature("volume", volume)
        self.energy = Feature("energy", energy)
        self.environment = Feature("environment", environment)
        self.surface_contribution = Feature("surface_contribution", surface_contribution)
        self.radius = Feature("radius", radius)
        self.input = Feature("input", None)
        self.output = Feature("output", None)

    @classmethod
    def from_json(cls, d: dict, n_max=64, l_max=64 ) -> 'Molecule':
        """Constructor to read everything from a dictionary file (json file is the expected origin)

        Since the format of this json file is not consistent, only fill in the features if they exist
        """
        try:
            surface = np.array([d["SURFACE"]])
        except KeyError:
            surface = None

        try:
            volume = np.array([d["VOLUME"]])
        except KeyError:
            volume = None
        
        try:
            energy = np.array([d["ENERGY"]])
        except KeyError:
            energy = None
        
        try:
            path = d["PATH"]
        except KeyError:
            path = None

        # note that if this doesn't exist, then the rest cannot be initialized, so just return
        # with everything else set to None
        try:
            nat = d["NCAVITY"]
            n_max = min(d["n_max"], n_max)
            l_max = min(d["l_max"], l_max)
            num_descriptors = n_max * (n_max + 1) * l_max // 2
        except KeyError:
            return cls(path, surface, volume, energy, None, None, None)

        try:
            environment = np.zeros((nat, num_descriptors,), dtype=float, order='F')
            for index in np.ndindex(environment.shape):
                n, nprime, l = get_indices(index[1], l_max)
                environment[index] = d[str(index[0])]["desc"][n][nprime][l]
        except KeyError:
            environment = None
        
        try:
            surface_contribution = np.zeros((nat, 1,), dtype=float)
            for i in range(nat):
                surface_contribution[i] = d[str(i)]["EXPOSED_SURF"]
        except KeyError:
            surface_contribution = None

        try:
            radius = np.zeros((nat, 1,), dtype=float)
            for i in range(nat):
                radius[i] = d[str(i)]["R"]
        except KeyError:
            radius = None

        return cls(path, surface, volume, energy, environment, surface_contribution, radius)

    @classmethod
    def from_python(cls, d: dict, n_max=64, l_max=64 ) -> 'Molecule':
        """Constructor to read everything from a dictionary object

        Since the format of this json obj is not consistent, only fill in the features if they exist
        """
        try:
            surface = d["SURFACE"]
        except KeyError:
            surface = None

        try:
            volume = d["VOLUME"]
        except KeyError:
            volume = None
        
        try:
            energy = d["ENERGY"]
        except KeyError:
            energy = None
        
        try:
            path = d["PATH"]
        except KeyError:
            path = None

        try:
            environment = d["ENVIRONMENT"]
        except KeyError:
            environment = None

        try:
            surface_contribution = d["SUR_CONT"]
        except KeyError:
            surface_contribution = None

        try:
            radius = d["RADIUS"]
        except KeyError:
            radius = None

        return cls(path, surface, volume, energy, environment, surface_contribution, radius)

    def get_vector_size(self, labels: List[str]) -> int:
        """Returns the dimensionality of the feature space defined by `labels`

        Args:
            labels (List[str]): list of labels to be accounted for by the features

        Returns:
            int: the dimensionality of the feature space
        """
        shape = 0
        for label in labels:
            try:
                shape += getattr(self, label.lower()).val.shape[-1]
            except TypeError:
                print(label)
                raise
        return shape 
    
    def get_vector(self, labels: List[str]) -> np.ndarray:
        """Returns a numpy array that combines features defined by `labels`

        Note that atomic and molecular features are not compatible and this will force an
        early exit by the program. 

        Args:
            labels (List[str]): list of labels to be accounted for by the features

        Returns:
            np.ndarray: The concatenated array
        """
        ndim = 0
        for label in labels:
            shape = getattr(self, label.lower()).val.shape
            if ndim > 0 and len(shape) != ndim:
                # error! we are trying to mix atomic and molecular descriptors without converting the atomic part first
                print("attempting to combine atomic and molecular descriptors without cleaning!")
                quit()
            else:
                ndim = len(shape)
        
        if ndim == 1:
            return np.concatenate(tuple(getattr(self, label.lower()).val for label in labels), axis=0)
        else:
            return np.concatenate(tuple(getattr(self, label.lower()).val for label in labels), axis=1)

    def set_input(self, labels: List[str], combine: str):
        vec = self.get_vector(labels)
        if len(vec.shape) == 2:
            # combine atomic part
            if combine == "additive":
                self.input.val = np.sum(vec, axis=0)
            elif combine == "zeros":
                self.input.val = np.zeros((vec.shape[-1],))
            elif combine == "none":
                self.input.val = vec
            else:
                raise NotImplementedError
        else:
            self.input.val = vec
        
    def set_input_from_vector(self, vec: np.ndarray, combine: str):
        if len(vec.shape) == 2:
            if combine == "additive":
                self.input.val = np.sum(vec, axis=0)
            elif combine == "none":
                self.input.val = vec
            else:
                raise NotImplementedError
        else:
            self.input.val = vec        

    def set_output(self, labels: List[str]):
        vec = self.get_vector(labels)
        self.output.val = vec

    def set_output_from_vector(self, vec: np.ndarray):
        self.output.val = vec

