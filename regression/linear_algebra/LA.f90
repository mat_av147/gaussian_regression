!Matrix determinant calculated by finding the eigenvalues
!and multiplying them
subroutine md( A, n, det )
    implicit none
    integer, intent(in) :: n
    real*8, intent(inout) :: A(n,n)
    real*8, intent(out) :: det
    real*8 :: eigv(n), WORK(3*n-1)
    integer :: info, i
    external dsyev
    det = 1.d0

    call dsyev( 'N', 'L', n, A, n, eigv, WORK, 3*n-1, info )

    do i=1,n
        det = det*eigv(i)
    end do

    return
end subroutine

!Matrix inverse calculated by LU factorization
!Used by scipy
subroutine mi_lu(A,n,B)
    implicit none
    integer, intent(in) :: n
    real*8, intent(inout) :: A(n,n)
    real*8, intent(out) :: B(n,n)
    integer :: ipiv(n), info
    real*8 :: work(n*n)
    external dgetri, dgetrf !LU factorization routines

    call dgetrf( n, n, A, n, ipiv, info )
    if ( .not. info == 0 ) return
    call dgetri( n, A, n, ipiv, work, n*n, info )
    if ( .not. info == 0 ) return
    B = A
    call zero(B,n)
    return

end subroutine

!Matrix inverse calculated by Cholesky factorization
subroutine mi_cd(A,n,B)
    implicit none
    integer, intent(in) :: n
    real*8, intent(inout) :: A(n,n)
    real*8, intent(out) :: B(n,n)
    integer :: info, i, j
    external dpotri, dpotrf !Cholesky factorization routines
    
    call dpotrf( 'L', n, A, n, info )
    if ( .not. info == 0 ) then
        A = 404.d0
        return
    end if
    call dpotri( 'L', n, A, n, info )
    if ( .not. info == 0 ) then
        A = 404.2d0
        return
    end if

    !dpotri returns the lower portion of the inverse matrix
    !Here I return the full inverse matrix
    do i=1,n
        do j=1,n
            if ( j <= i ) then
                B(i,j) = A(i,j)
            else
                B(i,j) = A(j,i)
            end if
        end do
    end do
    call zero(B,n)
    return

end subroutine

subroutine split_mat( A, n1, n2, B )
    implicit none
    integer, intent(in) :: n1, n2
    real*8 , intent(inout) :: A(n1+n2,n1+n2)
    real*8, intent(out) :: B(n1+n2,n1+n2)
    real*8, dimension(n1,n1) :: P, Pinv, N, Ntemp
    real*8, dimension(n2,n2) :: S, St, Sinv
    real*8 :: R(n1,n2), Q(n2,n1), Rt(n2,n1), Qt(n1,n2), temp(n2,n1)

    if ( n2 == 1 ) then
        print *, 'Use other inverse methods'
        return
    end if

    P = A(1:n1,1:n1)
    call mi_cd(P,n1,Pinv)
    P = A(1:n1,1:n1)
    R = A(n1+1:,1:n1)
    Q = Transpose(R)
    S = A(n1+1:,n1+1:)
    call mi_cd(S,n2,Sinv)
    S = A(n1+1:,n1+1:)
    temp = Matmul(Q,Sinv)
    Ntemp = P - Matmul(temp,R)
    call mi_cd(Ntemp,n1,N)
    
    Qt = -Matmul(Matmul(N,Q),Sinv)
    Rt = -Matmul(Matmul(Sinv,R),N)
    St = Sinv + Matmul(Matmul(Matmul(Matmul(Sinv,R),N),Q),Sinv)

    B(1:n1,1:n1) = N
    B(1:n1,n1+1:) = Qt
    B(n1+1:,1:n1) = Rt
    B(n1+1:,n1+1:) = St
    call zero(B,n1+n2)

    return
end subroutine

subroutine zero( A, n )
    implicit none
    integer, intent(in) :: n
    real*8, intent(inout) :: A(n,n)
    integer :: i, j

    do i=1,n
        do j=1,n
            if ( abs(A(i,j)) < 1.d-12 ) A(i,j) = 0.d0
        end do
    end do
    return
end subroutine

SUBROUTINE mm(A, B, C, i, j, k)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: i, j, k
    REAL(8), INTENT(IN) :: A(i, j)
    REAL(8), INTENT(IN) :: B(j, k)
    REAL(8), INTENT(OUT) :: C(i, k)
    C = MATMUL(A, B)
    RETURN
END SUBROUTINE
