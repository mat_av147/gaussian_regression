import numpy as np

def normalize_covariance(cov: np.ndarray) -> None:
    assert cov.ndim == 2
    for idx, val in np.ndenumerate(cov):
        cov[idx] /= np.sqrt(cov[idx[0], idx[0]] * cov[idx[1], idx[1]])