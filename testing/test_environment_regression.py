import unittest
import numpy as np

from regression.utility.dataset import molecular_distribution
from regression.utility.descriptor import *
from regression.learning.linear import LinearRegression
from regression.utility.distribute import get_random_indices

class TestEnvironmentRegression(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestEnvironmentRegression, self).__init__(*args, **kwargs)
        # generate an initial dataset
        self.n = 60
        self.nfx = 6
        self.natmax = 8
        self.dataset, self.beta, self.nat, self.xlabels, self.ylabels = molecular_distribution(
            self.n, self.nfx, self.natmax, 0.01, seed=0)

        self.rtol = 1e-3
        self.atol = 1e-3

        self.yinit = None

        self.indices = get_random_indices(self.n, 5, seed=0)
        self.regression = LinearRegression(self.xlabels, self.ylabels)    
    
    def set_output(self):
        for d in self.dataset:
            d.set_output(self.ylabels)

        self.yinit = get_feature_from_list(self.dataset, "OUTPUT")

    # The following methods test the methods for a single descriptor

    def test_vector_sizes(self):
        self.assertEqual(self.dataset[0].get_vector_size(self.xlabels), self.nfx)
        self.assertEqual(self.dataset[0].get_vector_size(self.ylabels), 1)

    # Test the machine learning methods

    def test_regression(self):
        self.set_output()

        self.regression.load_data(self.dataset, add_bias=False, **self.indices)

        # After normalization expect the norm to be equal to sqrt(n) and the mean to be zero
        # Now a bias is not required in simple regression

        # check that the mean is zero..
        self.assertAlmostEqual(np.mean(self.regression.x[self.indices["training_indices"]]), 0.0, 6)
        self.assertAlmostEqual(np.mean(self.regression.y[self.indices["training_indices"]]), 0.0, 6)

        beta, epsilon = self.regression.calc_weights(**self.indices)

        # check weights and biases
        print("\npredicted weights and biases: {}, {:6.3f}".format(beta[:, 0], epsilon[0]))
        print("actual weights and biases: {}, 0.0".format(self.beta.T[0]))

        # check prediction
        self.regression.update_prediction(**self.indices)
        print("\npredicted y")
        print(self.regression.get_prediction().T[0][self.indices["testing_indices"]])
        print("actual y")
        print(self.yinit.T[0][self.indices["testing_indices"]])

        # check objective function
        print("\nobjective", self.regression.get_objective(**self.indices))

if __name__ == '__main__':
    unittest.main()