import unittest
import numpy as np

from regression.utility.dataset import molecular_distribution
from regression.utility.descriptor import *
from regression.kernel.dot import MoleculeDotKernel
from regression.kernel.squared_exponential import MoleculeSquaredExponentialKernel
from regression.learning.gaussian import Gaussian
from regression.learning.linear import LinearRegression
from regression.utility.distribute import get_random_indices
from regression.optimization.gradient_descent import optimize_parameters, parameter_maximum, parameter_sweep

class TestGaussian(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestGaussian, self).__init__(*args, **kwargs)
        # generate an initial dataset
        self.n = 60
        self.nfx = 4
        self.natmax = 1
        self.dataset, self.beta, self.nat, self.xlabels, self.ylabels = molecular_distribution(
            self.n, self.nfx, self.natmax, 0.01, seed=13)

        self.rtol = 1e-3
        self.atol = 1e-3

        self.gp = Gaussian(self.xlabels, self.ylabels, None, 0.4)
        
        self.indices = get_random_indices(self.n, 5, seed=1)
        yref = get_feature_from_list(self.dataset, "energy")
        self.gp.load_data(self.dataset, **self.indices)


    def perform_gaussian(self):
        cov = self.gp.k(self.dataset, self.dataset)

        # test for symmetry
        self.assertTrue(np.all(np.isclose(cov, cov.T)))

        ## POSTERIOR
        yref = get_feature_from_list(self.dataset, "output")
        ytest = yref[self.indices["testing_indices"]]
        pmean, pcov, pmix = self.gp.posterior(self.dataset, self.dataset, **self.indices)
        pcov = np.diagonal(pcov)

        print("before optimization")
        err = self.gp.error_function(self.dataset, verbose=1, **self.indices)
        self.gp.cost_function(self.dataset)
        print("error = {:12.6f}".format(err))

    def optimize(self):
        #min_x = parameter_maximum(self.dataset, self.gp, 0, 2.5, 1.0, 5, 5)

        #self.gp.kernel = MoleculeDotKernel(min_x, 4)
        #print("gp_params after minimization", self.gp.get_param_values())
        optimize_parameters(self.dataset, self.gp, 1)
        
        ## POSTERIOR
        yref = get_feature_from_list(self.dataset, "output")
        ytest = yref[self.indices["testing_indices"]]
        pmean, pcov, pmix = self.gp.posterior(self.dataset, self.dataset, **self.indices)
        pcov = np.diagonal(pcov)

        print("after optimization")
        err = self.gp.error_function(self.dataset, verbose=1, **self.indices)
        self.gp.cost_function(self.dataset)
        print("error = {:12.6f}".format(err))

    def test_dot_kernel(self):
        self.gp.kernel = MoleculeDotKernel(1.5, 4)
        self.gp.kernel.set_sensitivity("signal_variance", 0.01)
        self.gp.set_sensitivity("noise_variance", 0.001)
        print("gp_params", self.gp.get_param_values())
        self.perform_gaussian()
        self.optimize()
    
    def test_sqexp_kernel(self):
        self.gp.kernel = MoleculeSquaredExponentialKernel(1.5, 4)
        self.gp.kernel.set_sensitivity("signal_variance", 0.01)
        self.gp.set_sensitivity("noise_variance", 0.001)
        print("gp_params", self.gp.get_param_values())
        self.perform_gaussian()
        self.optimize()




if __name__ == '__main__':
    unittest.main()