import unittest
import numpy as np

from regression.utility.dataset import molecular_distribution
from regression.utility.descriptor import *
from regression.learning.linear import LinearRegression
from regression.utility.distribute import get_indices

class TestStatistics(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestStatistics, self).__init__(*args, **kwargs)
        # generate an initial dataset
        self.n = 20
        self.nfx = 5
        self.natmax = 5
        self.dataset, self.beta, self.nat, self.xlabels, self.ylabels = molecular_distribution(
            self.n, self.nfx, self.natmax, 0.01, seed=0)
        self.rtol = 1e-3
        self.atol = 1e-3

        self.regression = LinearRegression(self.xlabels, self.ylabels)

    def test_mean_norm(self):
        indices = get_indices(self.n, 2, 0)
        labels = self.xlabels
        print("labels: {}".format(labels))
        mean, norm = get_mean_norm(self.dataset, labels, **indices)
        for i, mol in enumerate(self.dataset):
            print("mol {}\n{}".format(i, mol.environment.val))
        print("mean: {}".format(mean))
        print("norm: {}".format(norm))


if __name__ == '__main__':
    unittest.main()