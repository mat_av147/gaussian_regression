from regression.kernel.squared_exponential import SquaredExponentialKernel
import unittest
import numpy as np
from scipy.stats import multivariate_normal
import matplotlib.pyplot as plt

from regression.utility.dataset import simple_linear_distribution
from regression.utility.descriptor import *
from regression.kernel.dot import DotKernel
from regression.kernel.squared_exponential import SquaredExponentialKernel
from regression.learning.gaussian import Gaussian
from regression.utility.distribute import get_random_indices

class TestSimpleRegression(unittest.TestCase):
    """Unit test for the simple regression case y = x * beta

    where x and y are one dimensional vectors of length `n` where `n` is the number of molecules in the full dataset
    """
    def __init__(self, *args, **kwargs):
        super(TestSimpleRegression, self).__init__(*args, **kwargs)
        # generate an initial dataset
        self.n = 100
        self.dataset, self.beta, self.xlabels, self.ylabels, self.x = simple_linear_distribution(self.n, 0.1, seed=0)
        self.rtol = 1e-3
        self.atol = 1e-3

        self.gp = Gaussian(self.xlabels, self.ylabels, None, 0.1)

        self.indices = get_random_indices(self.n, 20)
        self.gp.load_data(self.dataset, **self.indices)

        self.y = self.x @ self.beta
        self.x -= self.gp.xmean
        self.y -= self.gp.ymean
        self.y *= self.gp.yscale

        #print("X")
        #print_feature_from_list(self.dataset, "INPUT")
        #print("Y")
        #print_feature_from_list(self.dataset, "OUTPUT")

    def perform_gaussian(self):
        _, ax = plt.subplots(2)
        
        cov = self.gp.k(self.dataset, self.dataset, normalize=True)
        jitter = np.eye(self.n) * 1e-6
        mean = np.zeros(self.n)

        # test for symmetry
        self.assertTrue(np.all(np.isclose(cov, cov.T)))

        ## PRIOR
        samples = []
        probabilities = []

        # print the prior distribution
        for _ in range(20):
            y = multivariate_normal.rvs(mean=mean, cov=cov)
            p = multivariate_normal.pdf(y, mean=mean, cov=cov+jitter)

            samples.append(y)
            probabilities.append(p)
     
        probabilities = np.array(probabilities)
        min_prob, max_prob = np.min(probabilities), np.max(probabilities)
        probabilities = (probabilities - min_prob) / (max_prob - min_prob)
    
        # Plotting.
        x = self.x
        for y, prob in zip(samples, probabilities):
            ax[0].plot(x, y, alpha=prob * 2)

        ## POSTERIOR
        pmean, pcov, pmix = self.gp.posterior(self.dataset, self.dataset, **self.indices)
        pcov = np.diagonal(pcov)

        # Plot posterior mean and variance.
        x = self.x[self.indices["testing_indices"], :]
        y_plot = self.y[self.indices["testing_indices"], :]
        ax[1].plot(x, pmean, color='red')
        ax[1].plot(x, pmean, '.')
        ax[1].fill_between(x[:, 0],
                           pmean[:, 0] - 1.96 * np.sqrt(pcov),
                           pmean[:, 0] + 1.96 * np.sqrt(pcov),
                           color='C0', alpha=0.2)
        ax[1].plot(x, y_plot)
        plt.show()

    def test_dot_kernel(self):
        self.gp.kernel = DotKernel(0.0, 1)
        self.perform_gaussian()

    def test_sqexp_kernel(self):
        self.gp.kernel = SquaredExponentialKernel(10.0, 0.4)
        self.perform_gaussian()
    
if __name__ == '__main__':
    unittest.main()