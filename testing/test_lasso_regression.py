import unittest
import numpy as np
import matplotlib.pyplot as plt

from regression.utility.dataset import multi_linear_distribution
from regression.utility.descriptor import *
from regression.learning.lasso import LassoRegression
from regression.utility.distribute import get_indices

class TestLassoRegression(unittest.TestCase):
    """Unit test for the ridge regression case y = x * beta

    where y is a one dimensional vectors of length `n` where `n` is the number of molecules in the full dataset
    x is a multidimensional vector of length `n` * `nfx` where `nfx` is the number of input features
    """
    def __init__(self, *args, **kwargs):
        super(TestLassoRegression, self).__init__(*args, **kwargs)
        # generate an initial dataset
        self.n = 50
        self.nfx = 1
        self.dataset, self.beta, self.xlabels, self.ylabels = multi_linear_distribution(self.n, self.nfx, 0.0, seed=0)
        self.rtol = 1e-3
        self.atol = 1e-3

        self.xinit = None
        self.yinit = None
        self.plot = True
    
    def set_input_output(self):
        for d in self.dataset:
            d.set_input(self.xlabels, "additive")
            d.set_output(self.ylabels)

    # The following methods test the methods for a single descriptor

    def test_vector_sizes(self):
        self.assertEqual(self.dataset[0].get_vector_size(self.xlabels), self.nfx)
        self.assertEqual(self.dataset[0].get_vector_size(self.ylabels), 1)

    # The following methods test the methods for a list of descriptors. These are stored in utility.descriptor

    def test_get_feature(self):
        self.set_input_output()
        x = get_feature_from_list(self.dataset, "INPUT")
        y = get_feature_from_list(self.dataset, "OUTPUT")

        self.xinit = x
        self.yinit = y
        
        self.assertEqual(x.shape, (self.n, self.nfx))
        self.assertEqual(y.shape, (self.n, 1))

    # Test the machine learning methods

    def test_regression_ortho(self):
        print("\n----------\nORTHO TEST\n----------")
        self.test_get_feature()
        regression = LassoRegression(self.xlabels, self.ylabels, "ORTHO", lagrange=0.001)

        indices = get_indices(self.n, 1, 0)
        regression.load_data(self.dataset, add_bias=False, **indices)

        # After normalization expect the norm to be equal to sqrt(n) and the mean to be zero
        # Now a bias is not required in simple regression

        # check that the mean is zero..
        self.assertAlmostEqual(np.mean(regression.x[indices["training_indices"]]), 0.0, 6)
        self.assertAlmostEqual(np.mean(regression.y[indices["training_indices"]]), 0.0, 6)

        # check that the normalization is as expected..
        #print(np.linalg.norm(self.regression.x[indices["training_indices"]], axis=0))
        self.assertTrue(np.allclose(
            np.linalg.norm(regression.x[indices["training_indices"]], axis=0), 
            np.ones(self.nfx) * (self.n - 1) ** 0.5,
            self.rtol, self.atol))
        self.assertTrue(np.allclose(
            np.linalg.norm(regression.y[indices["training_indices"]], axis=0), 
            np.ones(self.nfx) * (self.n - 1) ** 0.5,
            self.rtol, self.atol))

        beta, epsilon = regression.calc_weights(**indices)

        # check weights and biases
        print("\npredicted weights and biases: {}, {:6.3f}".format(beta[:, 0], epsilon[0]))
        print("actual weights and biases: {}, 0.0".format(self.beta.T[0]))

        # check prediction
        regression.update_prediction(**indices)
        print("\npredicted y")
        print(regression.get_prediction().T[0])
        print("actual y")
        print(self.yinit.T[0])

        # check objective function
        print("\nobjective", regression.get_objective(**indices))
    
    def test_regression_gd(self):
        print("\n-------\nGD TEST\n-------")
        self.test_get_feature()
        regression = LassoRegression(self.xlabels, self.ylabels, "GD", lagrange=0.001)

        indices = get_indices(self.n, 1, 0)
        regression.load_data(self.dataset, add_bias=False, **indices)

        # After normalization expect the norm to be equal to sqrt(n) and the mean to be zero
        # Now a bias is not required in simple regression

        # check that the mean is zero..
        self.assertAlmostEqual(np.mean(regression.x[indices["training_indices"]]), 0.0, 6)
        self.assertAlmostEqual(np.mean(regression.y[indices["training_indices"]]), 0.0, 6)

        # check that the normalization is as expected..
        #print(np.linalg.norm(self.regression.x[indices["training_indices"]], axis=0))
        self.assertTrue(np.allclose(
            np.linalg.norm(regression.x[indices["training_indices"]], axis=0), 
            np.ones(self.nfx) * (self.n - 1) ** 0.5,
            self.rtol, self.atol))
        self.assertTrue(np.allclose(
            np.linalg.norm(regression.y[indices["training_indices"]], axis=0), 
            np.ones(self.nfx) * (self.n - 1) ** 0.5,
            self.rtol, self.atol))

        beta, epsilon = regression.calc_weights(self.plot, **indices)

        # check weights and biases
        print("\npredicted weights and biases: {}, {:6.3f}".format(beta[:, 0], epsilon[0]))
        print("actual weights and biases: {}, 0.0".format(self.beta.T[0]))

        # check prediction
        regression.update_prediction(**indices)
        print("\npredicted y")
        print(regression.get_prediction().T[0])
        print("actual y")
        print(self.yinit.T[0])

        # check objective function
        print("\nobjective", regression.get_objective(**indices))

    def test_regression_ista(self):
        print("\n---------\nISTA TEST\n---------")
        print(self.beta)
        self.test_get_feature()
        regression = LassoRegression(self.xlabels, self.ylabels, "ISTA", lagrange=0.001)

        indices = get_indices(self.n, 1, 0)
        regression.load_data(self.dataset, add_bias=False, **indices)

        # After normalization expect the norm to be equal to sqrt(n) and the mean to be zero
        # Now a bias is not required in simple regression

        # check that the mean is zero..
        self.assertAlmostEqual(np.mean(regression.x[indices["training_indices"]]), 0.0, 6)
        self.assertAlmostEqual(np.mean(regression.y[indices["training_indices"]]), 0.0, 6)

        # check that the normalization is as expected..
        #print(np.linalg.norm(self.regression.x[indices["training_indices"]], axis=0))
        self.assertTrue(np.allclose(
            np.linalg.norm(regression.x[indices["training_indices"]], axis=0), 
            np.ones(self.nfx) * (self.n - 1) ** 0.5,
            self.rtol, self.atol))
        self.assertTrue(np.allclose(
            np.linalg.norm(regression.y[indices["training_indices"]], axis=0), 
            np.ones(self.nfx) * (self.n - 1) ** 0.5,
            self.rtol, self.atol))

        beta, epsilon = regression.calc_weights(self.plot, **indices)

        # check weights and biases
        print("\npredicted weights and biases: {}, {:6.3f}".format(beta[:, 0], epsilon[0]))
        print("actual weights and biases: {}, 0.0".format(self.beta.T[0]))

        # check prediction
        regression.update_prediction(**indices)
        print("\npredicted y")
        print(regression.get_prediction().T[0])
        print("actual y")
        print(self.yinit.T[0])

        # check objective function
        print("\nobjective", regression.get_objective(**indices))

    def test_regression_fista(self):
        print("\n----------\nFISTA TEST\n----------")
        self.test_get_feature()
        regression = LassoRegression(self.xlabels, self.ylabels, "FISTA", lagrange=0.001)

        indices = get_indices(self.n, 1, 0)
        regression.load_data(self.dataset, add_bias=False, **indices)

        # After normalization expect the norm to be equal to sqrt(n) and the mean to be zero
        # Now a bias is not required in simple regression

        # check that the mean is zero..
        self.assertAlmostEqual(np.mean(regression.x[indices["training_indices"]]), 0.0, 6)
        self.assertAlmostEqual(np.mean(regression.y[indices["training_indices"]]), 0.0, 6)

        # check that the normalization is as expected..
        #print(np.linalg.norm(self.regression.x[indices["training_indices"]], axis=0))
        self.assertTrue(np.allclose(
            np.linalg.norm(regression.x[indices["training_indices"]], axis=0), 
            np.ones(self.nfx) * (self.n - 1) ** 0.5,
            self.rtol, self.atol))
        self.assertTrue(np.allclose(
            np.linalg.norm(regression.y[indices["training_indices"]], axis=0), 
            np.ones(self.nfx) * (self.n - 1) ** 0.5,
            self.rtol, self.atol))

        beta, epsilon = regression.calc_weights(self.plot, **indices)

        # check weights and biases
        print("\npredicted weights and biases: {}, {:6.3f}".format(beta[:, 0], epsilon[0]))
        print("actual weights and biases: {}, 0.0".format(self.beta.T[0]))

        # check prediction
        regression.update_prediction(**indices)
        print("\npredicted y")
        print(regression.get_prediction().T[0])
        print("actual y")
        print(self.yinit.T[0])

        # check objective function
        print("\nobjective", regression.get_objective(**indices))

if __name__ == '__main__':
    unittest.main()