from regression.kernel.squared_exponential import SquaredExponentialKernel
import unittest
import numpy as np
from regression.kernel.dot import DotKernel
from regression.kernel.squared_exponential import SquaredExponentialKernel

class TestKernel(unittest.TestCase):
    # test a few functions in the kernel since we don't really need most of them.. yet
    def test_dot_kernel(self):
        print("\nDOT KERNEL")
        k = DotKernel(0., 1.)
        a = np.random.randint(0, 5, (1,))
        b = np.random.randint(0, 5, (1,))
        c = k.calc_covariance(a, b)
        print(a, b, c)
        a = np.random.randint(0, 5, (3,))
        b = np.random.randint(0, 5, (3,))
        c = k.calc_covariance(a, b)
        print(a, b, c)

    def test_sqexp_kernel(self):
        print("\nSQ EXP KERNEL")
        k = SquaredExponentialKernel(1., 1.)
        a = np.random.randint(0, 5, (1,))
        b = np.random.randint(0, 5, (1,))
        c = k.calc_covariance(a, b)
        print(a, b, c)
        a = np.random.randint(0, 5, (3,))
        b = np.random.randint(0, 5, (3,))
        c = k.calc_covariance(a, b)
        print(a, b, c)

if __name__ == '__main__':
    unittest.main()