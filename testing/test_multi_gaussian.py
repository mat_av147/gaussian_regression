import unittest
import numpy as np
from scipy.stats import multivariate_normal
import matplotlib.pyplot as plt

from regression.utility.dataset import multi_linear_distribution
from regression.utility.descriptor import *
from regression.kernel.dot import DotKernel
from regression.kernel.squared_exponential import SquaredExponentialKernel
from regression.learning.gaussian import Gaussian
from regression.utility.distribute import get_indices, get_random_indices
from regression.optimization.gradient_descent import optimize_parameters, parameter_sweep

class TestSimpleRegression(unittest.TestCase):
    """Unit test for the simple regression case y = x * beta

    where x and y are one dimensional vectors of length `n` where `n` is the number of molecules in the full dataset
    """
    def __init__(self, *args, **kwargs):
        super(TestSimpleRegression, self).__init__(*args, **kwargs)
        # generate an initial dataset
        self.n = 50
        self.nfx = 3
        self.dataset, self.beta, self.xlabels, self.ylabels = multi_linear_distribution(self.n, self.nfx, 0.001, seed=0)
        self.rtol = 1e-3
        self.atol = 1e-3

        self.gp = Gaussian(self.xlabels, self.ylabels, None, 0.1)

        self.indices = get_random_indices(self.n, 5, 0)
        self.gp.load_data(self.dataset, **self.indices)

        self.y = get_feature_from_list(self.dataset, "output")
        #print(self.y)


    def perform_gaussian(self):        
        cov = self.gp.k(self.dataset, self.dataset)
        jitter = np.eye(self.n) * 1e-6
        mean = np.zeros(self.n)

        # test for symmetry
        self.assertTrue(np.all(np.isclose(cov, cov.T)))

        ## PRIOR
        samples = []
        probabilities = []

        # print the prior distribution
        for _ in range(20):
            y = multivariate_normal.rvs(mean=mean, cov=cov)
            p = multivariate_normal.pdf(y, mean=mean, cov=cov+jitter)

            samples.append(y)
            probabilities.append(p)
     
        probabilities = np.array(probabilities)
        min_prob, max_prob = np.min(probabilities), np.max(probabilities)
        probabilities = (probabilities - min_prob) / (max_prob - min_prob)

        ## POSTERIOR
        yref = get_feature_from_list(self.dataset, "output")
        ytest = yref[self.indices["testing_indices"]]
        pmean, pcov, pmix = self.gp.posterior(self.dataset, self.dataset, **self.indices)
        pcov = np.diagonal(pcov)

        print("before optimization")
        err = self.gp.error_function(self.dataset, verbose=1, denorm=False, **self.indices)
        self.gp.cost_function(self.dataset)
        print("error = {:12.6f}".format(err))

    def optimize(self):
        optimize_parameters(self.dataset, self.gp, verbose=1)
        #parameter_sweep(self.dataset, self.gp, 0, (-1, 1), 50)
        
        ## POSTERIOR
        yref = get_feature_from_list(self.dataset, "output")
        ytest = yref[self.indices["testing_indices"]]
        pmean, pcov, pmix = self.gp.posterior(self.dataset, self.dataset, **self.indices)
        pcov = np.diagonal(pcov)

        print("after optimization")
        err = self.gp.error_function(self.dataset, verbose=1, denorm=False, **self.indices)
        self.gp.cost_function(self.dataset)
        print("error = {:12.6f}".format(err))

    def test_dot_kernel(self):
        self.gp.kernel = DotKernel(0.4, 1)
        self.gp.kernel.set_sensitivity("signal_variance", 5.0)
        self.gp.set_sensitivity("noise_variance", 0.0)
        self.perform_gaussian()
        self.optimize()
    
    def test_sqexp_kernel(self):
        self.gp.kernel = SquaredExponentialKernel([1.0] * self.nfx, 1.0)
        self.gp.kernel.set_sensitivity("lengthscale", 0.01)
        self.gp.kernel.set_sensitivity("signal_variance", 1e-4)
        self.gp.set_sensitivity("noise_variance", 0.0)
        self.perform_gaussian()
        self.optimize()
        
    
if __name__ == '__main__':
    unittest.main()